package awopquests.vadim99808.constants;

public enum RegionRule {
    EXCLUDE,
    EXCLUDE_ALL,
    INCLUDE_ALL,
    INCLUDE,
    STRICT
}
