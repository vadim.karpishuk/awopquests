package awopquests.vadim99808.constants;

public enum LevelChangeType {
    INCREASE,
    DECREASE
}
