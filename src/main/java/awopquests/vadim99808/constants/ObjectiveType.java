package awopquests.vadim99808.constants;

public enum ObjectiveType {
    KILL,
    BREAK,
    FISH,
    BREAK_ITEM,
    CONSUME,
    EGG_THROW,
    SHEAR,
    LEVEL_CHANGE,
    BLOCK_FERTILIZE,
    ENCHANT_ITEM
}
