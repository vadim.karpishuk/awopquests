package awopquests.vadim99808.constants;

public enum ObjectiveWinType {
    BY_MAX_AMOUNT_IN_TIME,
    BY_AMOUNT_IN_TIME
}
