package awopquests.vadim99808.utils;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.scheduler.BukkitRunnable;

public class EventCaller extends BukkitRunnable {

    private Event event;

    public EventCaller(Event event){
        this.event = event;
    }

    @Override
    public void run() {
        Bukkit.getServer().getPluginManager().callEvent(event);
    }
}
