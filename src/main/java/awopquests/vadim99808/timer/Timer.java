package awopquests.vadim99808.timer;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.service.QuestService;
import awopquests.vadim99808.storages.MainConfigurationStorage;
import awopquests.vadim99808.storages.QuestStorage;

import java.util.Random;

public class Timer extends Thread {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private QuestService questService = plugin.getQuestService();
    private MainConfigurationStorage mainConfigurationStorage = MainConfigurationStorage.getInstance();
    private QuestStorage questStorage = QuestStorage.getInstance();

    public void run(){
        while(true){
            try {
                Thread.sleep(1000 * mainConfigurationStorage.getSpawnDelay());
                if(new Random().nextInt(100) + 1 <= mainConfigurationStorage.getSpawnChance() && questStorage.getQuestMap().size() > 0){
                    questService.startRandomQuest();
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }

}
