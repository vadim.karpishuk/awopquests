package awopquests.vadim99808;

import openyaml.vadim99808.annotations.ConfigChild;
import openyaml.vadim99808.annotations.ConfigField;
import openyaml.vadim99808.annotations.ConfigFile;
import openyaml.vadim99808.annotations.ConfigPath;

import java.io.File;
import java.util.List;

@ConfigFile
public class Test {

    @ConfigField(name = "Test")
    private String test;

    @ConfigField(name = "Pidor")
    private String test1;

    @ConfigField(name = "Stas")
    private String test2;

    @ConfigField(name = "ChildTest")
    @ConfigChild(source = ChildTest.class)
    private List<ChildTest> childTest;

    @ConfigPath
    private File file;

    public Test(){
        this.file = new File(AWOPQuests.getInstance().getMainDirectory(), "test");
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getTest1() {
        return test1;
    }

    public void setTest1(String test1) {
        this.test1 = test1;
    }

    public String getTest2() {
        return test2;
    }

    public void setTest2(String test2) {
        this.test2 = test2;
    }

    public List<ChildTest> getChildTest() {
        return childTest;
    }

    public void setChildTest(List<ChildTest> childTest) {
        this.childTest = childTest;
    }
}
