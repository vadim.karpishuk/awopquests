package awopquests.vadim99808;

import awopquests.vadim99808.commandExecutors.*;
import awopquests.vadim99808.configurationLoaders.*;
import awopquests.vadim99808.entity.Award;
import awopquests.vadim99808.listeners.objective.*;
import awopquests.vadim99808.service.*;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import io.lumine.xikage.mythicmobs.MythicMobs;
import net.milkbowl.vault.economy.Economy;
import openyaml.vadim99808.initializers.ConfigLoader;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import awopquests.vadim99808.entity.objective.Objective;
import awopquests.vadim99808.entity.Quest;
import awopquests.vadim99808.listeners.*;
import awopquests.vadim99808.timer.Timer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

public class AWOPQuests extends JavaPlugin {

    private File mainDirectory;
    private File questsDirectory;
    private File objectiveDirectory;
    private File awardDirectory;
    private ConfigurationLoader<Objective> objectiveConfigurationLoader;
    private ConfigurationLoader<Quest> questConfigurationLoader;
    private ConfigurationLoader<Award> awardConfigurationLoader;
    private MainConfigurationLoader mainConfigurationLoader;
    private QuestService questService;
    private ObjectiveService objectiveService;
    private BroadcastService broadcastService;
    private AwardService awardService;
    private Timer timer;
    private static AWOPQuests instance;
    private MythicMobs mythicMobs;
    private Economy economy;
    private WorldGuardPlugin worldGuardPlugin;
    private WorldEditPlugin worldEditPlugin;
    private boolean firstInstall;

    @Override
    public void onEnable() {
        instance = this;
        pluginsIntegration();
        initDirectories();
        mainConfigurationLoader = new MainConfigurationLoader();
        mainConfigurationLoader.loadAll();
        objectiveConfigurationLoader = new ObjectiveConfigurationLoader();
        objectiveConfigurationLoader.loadAll();
        awardConfigurationLoader = new AwardConfigurationLoader();
        awardConfigurationLoader.loadAll();
        questConfigurationLoader = new QuestConfigurationLoader();
        questConfigurationLoader.loadAll();
        broadcastService = new BroadcastService();
        objectiveService = new ObjectiveService();
        questService = new QuestService();
        awardService = new AwardService();

        registerEvents();
        registerCommands();

        ConfigLoader<Test> configLoader = new ConfigLoader<>();
        @SuppressWarnings("unchecked")
        List<Test> test = (List<Test>) configLoader.loadConfig(Test.class);
        for(Test test1: test) {
            System.out.println("===============================================");
            System.out.println(test1.getTest());
            System.out.println(test1.getTest1());
            System.out.println(test1.getTest2());
            for(ChildTest childTest: test1.getChildTest()) {
                System.out.println(childTest.getName());
            }
        }

        timer = new Timer();
        timer.start();
    }

    @Override
    public void onDisable() {
        timer.interrupt();
        questService.stopAllActiveQuests();
    }

    private void pluginsIntegration(){
        try {
            mythicMobs = MythicMobs.inst();
        }catch (NoClassDefFoundError noClassDefFoundError){ }
        if(mythicMobs != null){
            getLogger().info("MythicMobs found!");
        }else{
            getLogger().info("MythicMobs not found!");
        }
        RegisteredServiceProvider<Economy> rsp = null;
        try {
            rsp = getServer().getServicesManager().getRegistration(Economy.class);
        }catch (NoClassDefFoundError noClassDefFoundError){ }
        if (rsp != null) {
            economy = rsp.getProvider();
        }
        if(economy != null){
            getLogger().info("Vault found!");
        }else{
            getLogger().info("Vault not found!");
        }
        worldGuardPlugin = (WorldGuardPlugin)getServer().getPluginManager().getPlugin("WorldGuard");
        if(worldGuardPlugin != null){
            getLogger().info("WorldGuard found!");
        }else{
            getLogger().info("WorldGuard not found!");
        }
        worldEditPlugin = (WorldEditPlugin)getServer().getPluginManager().getPlugin("WorldEdit");
        if(worldEditPlugin != null){
            getLogger().info("WorldEdit found!");
        }else{
            getLogger().info("WorldEdit not found!");
        }
    }

    private void registerEvents(){
        getServer().getPluginManager().registerEvents(new QuestAppearListener(), this);
        getServer().getPluginManager().registerEvents(new QuestDisappearListener(), this);
        getServer().getPluginManager().registerEvents(new QuestDoneListener(), this);
        getServer().getPluginManager().registerEvents(new KillObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new BreakObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new ObjectiveDoneListener(), this);
        getServer().getPluginManager().registerEvents(new ConsumeObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new FishObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new BreakItemObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new EggThrowObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new ShearObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new LevelChangeListener(), this);
        getServer().getPluginManager().registerEvents(new BlockFertilizeObjectiveListener(), this);
        getServer().getPluginManager().registerEvents(new EnchantItemObjectiveListener(), this);
    }

    private void registerCommands(){
        getCommand("queststart").setExecutor(new QuestStartCommandExecutor());
        getCommand("questlist").setExecutor(new ActiveQuestListCommandExecutor());
        getCommand("questreload").setExecutor(new QuestReloadCommandExecutor());
        getCommand("questinfo").setExecutor(new QuestInfoCommandExecutor());
        getCommand("queststop").setExecutor(new QuestStopCommandExecutor());
    }

    private void initDirectories(){
        mainDirectory = getDataFolder();
        if(!mainDirectory.exists()){
            firstInstall = true;
            mainDirectory.mkdir();
        }else{
            firstInstall = false;
        }
        questsDirectory = new File(mainDirectory, "Quests");
        if(!questsDirectory.exists()){
            questsDirectory.mkdir();
        }
        objectiveDirectory = new File(mainDirectory, "Objectives");
        if(!objectiveDirectory.exists()){
            objectiveDirectory.mkdir();
        }
        awardDirectory = new File(mainDirectory, "Awards");
        if(!awardDirectory.exists()){
            awardDirectory.mkdir();
        }
        if(firstInstall){
            initExampleConfigFiles();
        }
    }

    private void initExampleConfigFiles(){
        initExampleObjectives();
        initExampleAwards();
        initExampleQuests();
    }

    private void initExampleObjectives(){
        List<String> objectiveNames = Arrays.asList("ExampleBlockBreakObjective.yml", "ExampleBreakItemObjective.yml", "ExampleFishObjective.yml",
                "ExampleKillObjective.yml", "ExampleConsumeObjective.yml", "ExampleEggThrowObjective.yml", "ExampleShearObjective.yml",
                "ExampleLevelChangeObjective.yml", "ExampleBlockFertilizeObjective.yml", "ExampleEnchantItemObjective.yml");
        for(String name: objectiveNames){
            File file = new File(objectiveDirectory, name);
            InputStream inputStream = getClass().getResourceAsStream("/Objectives/" + name);
            try {
                Files.copy(inputStream, file.getAbsoluteFile().toPath());
            } catch (IOException e) {
                getLogger().warning("Something got wrong while making default objectives!");
            }
        }
    }

    private void initExampleAwards(){
        List<String> awardNames = Arrays.asList("ExampleMoneyAward.yml", "ExampleExtraMoneyAward.yml", "ExampleXpAward.yml",
                "ExampleExtraXpAward.yml", "ExampleItemAward.yml", "ExampleComplexAward.yml");
        for(String name: awardNames){
            File file = new File(awardDirectory, name);
            InputStream inputStream = getClass().getResourceAsStream("/Awards/" + name);
            try {
                Files.copy(inputStream, file.getAbsoluteFile().toPath());
            } catch (IOException e) {
                getLogger().warning("Something got wrong while making default awards!");
            }
        }
    }

    private void initExampleQuests(){
        List<String> questNames = Arrays.asList("ExampleBlockBreakQuest.yml", "ExampleBreakItemQuest.yml", "ExampleFishQuest.yml",
                "ExampleKillQuest.yml", "ExampleConsumeQuest.yml", "ExampleEggThrowQuest.yml", "ExampleShearQuest.yml",
                "ExampleLevelChangeQuest.yml", "ExampleBlockFertilizeQuest.yml", "ExampleEnchantItemQuest.yml");
        for(String name: questNames){
            File file = new File(questsDirectory, name);
            InputStream inputStream = getClass().getResourceAsStream("/Quests/" + name);
            try {
                Files.copy(inputStream, file.getAbsoluteFile().toPath());
            } catch (IOException e) {
                getLogger().warning("Something got wrong while making default quests!");
            }
        }
    }

    public void reloadConfigurations(){
        initDirectories();
        mainConfigurationLoader.loadAll();
        objectiveConfigurationLoader.loadAll();
        awardConfigurationLoader.loadAll();
        questConfigurationLoader.loadAll();
    }

    public static AWOPQuests getInstance(){
        return instance;
    }

    public File getMainDirectory() {
        return mainDirectory;
    }

    public File getQuestsDirectory() {
        return questsDirectory;
    }

    public File getObjectiveDirectory(){
        return objectiveDirectory;
    }

    public QuestService getQuestService() {
        return questService;
    }

    public BroadcastService getBroadcastService() {
        return broadcastService;
    }

    public MythicMobs getMythicMobs() {
        return mythicMobs;
    }

    public Economy getEconomy() {
        return economy;
    }

    public WorldGuardPlugin getWorldGuardPlugin() {
        return worldGuardPlugin;
    }

    public WorldEditPlugin getWorldEditPlugin() {
        return worldEditPlugin;
    }

    public File getAwardDirectory() {
        return awardDirectory;
    }

    public AwardService getAwardService() {
        return awardService;
    }

    public ObjectiveService getObjectiveService() {
        return objectiveService;
    }
}
