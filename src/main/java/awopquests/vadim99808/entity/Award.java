package awopquests.vadim99808.entity;

import java.util.List;
import java.util.Optional;

public class Award extends EntityAbstract {
    private Optional<Integer> maxMoney;
    private Optional<Integer> minMoney;
    private Optional<Integer> moneyChance;
    private Optional<List<ItemMap>> itemMapList;
    private Optional<Integer> maxXpLevel;
    private Optional<Integer> minXpLevel;
    private Optional<Integer> xpChance;

    public Optional<Integer> getMaxMoney() {
        return maxMoney;
    }

    public void setMaxMoney(Optional<Integer> maxMoney) {
        this.maxMoney = maxMoney;
    }

    public Optional<Integer> getMinMoney() {
        return minMoney;
    }

    public void setMinMoney(Optional<Integer> minMoney) {
        this.minMoney = minMoney;
    }

    public Optional<Integer> getMoneyChance() {
        return moneyChance;
    }

    public void setMoneyChance(Optional<Integer> moneyChance) {
        this.moneyChance = moneyChance;
    }

    public Optional<List<ItemMap>> getItemMapList() {
        return itemMapList;
    }

    public void setItemMapList(Optional<List<ItemMap>> itemMapList) {
        this.itemMapList = itemMapList;
    }

    public Optional<Integer> getMaxXpLevel() {
        return maxXpLevel;
    }

    public void setMaxXpLevel(Optional<Integer> maxXpLevel) {
        this.maxXpLevel = maxXpLevel;
    }

    public Optional<Integer> getMinXpLevel() {
        return minXpLevel;
    }

    public void setMinXpLevel(Optional<Integer> minXpLevel) {
        this.minXpLevel = minXpLevel;
    }

    public Optional<Integer> getXpChance() {
        return xpChance;
    }

    public void setXpChance(Optional<Integer> xpChance) {
        this.xpChance = xpChance;
    }
}
