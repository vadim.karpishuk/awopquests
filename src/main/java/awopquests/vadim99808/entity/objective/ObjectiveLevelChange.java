package awopquests.vadim99808.entity.objective;

import awopquests.vadim99808.constants.LevelChangeType;
import awopquests.vadim99808.constants.ObjectiveType;

public class ObjectiveLevelChange extends Objective {
    private static ObjectiveType objectiveType = ObjectiveType.LEVEL_CHANGE;
    private LevelChangeType levelChangeType;

    public LevelChangeType getLevelChangeType() {
        return levelChangeType;
    }

    public void setLevelChangeType(LevelChangeType levelChangeType) {
        this.levelChangeType = levelChangeType;
    }
}
