package awopquests.vadim99808.entity.objective;

import awopquests.vadim99808.constants.ObjectiveType;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public class ObjectiveKill extends Objective {
    private static ObjectiveType objectiveType = ObjectiveType.KILL;
    private EntityType entityType;
    private Optional<ItemStack> itemHeld;

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Optional<ItemStack> getItemHeld() {
        return itemHeld;
    }

    public void setItemHeld(Optional<ItemStack> itemHeld) {
        this.itemHeld = itemHeld;
    }
}
