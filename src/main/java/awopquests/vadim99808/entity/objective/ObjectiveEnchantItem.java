package awopquests.vadim99808.entity.objective;

import awopquests.vadim99808.constants.ObjectiveType;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.Map;
import java.util.Optional;

public class ObjectiveEnchantItem extends Objective {
    private ObjectiveType objectiveType = ObjectiveType.ENCHANT_ITEM;
    private Material material;
    private Optional<Map<Enchantment, Integer>> enchantsMap;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Optional<Map<Enchantment, Integer>> getEnchantsMap() {
        return enchantsMap;
    }

    public void setEnchantsMap(Optional<Map<Enchantment, Integer>> enchantsMap) {
        this.enchantsMap = enchantsMap;
    }
}
