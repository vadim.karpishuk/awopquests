package awopquests.vadim99808.entity.objective;

import awopquests.vadim99808.constants.ObjectiveType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public class ObjectiveConsume extends Objective{
    private ObjectiveType objectiveType = ObjectiveType.CONSUME;
    private Material material;
    private Optional<ItemStack> itemHeld;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Optional<ItemStack> getItemHeld() {
        return itemHeld;
    }

    public void setItemHeld(Optional<ItemStack> itemHeld) {
        this.itemHeld = itemHeld;
    }
}
