package awopquests.vadim99808.entity.objective;

import awopquests.vadim99808.constants.ObjectiveType;
import awopquests.vadim99808.constants.ObjectiveWinType;
import awopquests.vadim99808.constants.RegionRule;
import awopquests.vadim99808.entity.EntityAbstract;
import org.bukkit.World;

import java.util.List;
import java.util.Optional;

public abstract class Objective extends EntityAbstract {
    private ObjectiveType objectiveType;
    private ObjectiveWinType objectiveWinType;
    private Optional<Integer> maxAmount;
    private Optional<Integer> minAmount;
    private int seconds;
    private List<World> worldList;
    private Optional<Integer> amountOfPossiblePassedPlayers;
    private RegionRule regionRule;
    private Optional<List<String>> regionNames;

    public ObjectiveType getObjectiveType() {
        return objectiveType;
    }

    public void setObjectiveType(ObjectiveType objectiveType) {
        this.objectiveType = objectiveType;
    }

    public List<World> getWorldList() {
        return worldList;
    }

    public void setWorldList(List<World> worldList) {
        this.worldList = worldList;
    }

    public ObjectiveWinType getObjectiveWinType() {
        return objectiveWinType;
    }

    public void setObjectiveWinType(ObjectiveWinType objectiveWinType) {
        this.objectiveWinType = objectiveWinType;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public Optional<Integer> getAmountOfPossiblePassedPlayers() {
        return amountOfPossiblePassedPlayers;
    }

    public void setAmountOfPossiblePassedPlayers(Optional<Integer> amountOfPossiblePassedPlayers) {
        this.amountOfPossiblePassedPlayers = amountOfPossiblePassedPlayers;
    }

    public Optional<Integer> getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Optional<Integer> maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Optional<Integer> getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Optional<Integer> minAmount) {
        this.minAmount = minAmount;
    }

    public RegionRule getRegionRule() {
        return regionRule;
    }

    public void setRegionRule(RegionRule regionRule) {
        this.regionRule = regionRule;
    }

    public Optional<List<String>> getRegionNames() {
        return regionNames;
    }

    public void setRegionNames(Optional<List<String>> regionNames) {
        this.regionNames = regionNames;
    }
}
