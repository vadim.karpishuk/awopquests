package awopquests.vadim99808.entity.objective;

import awopquests.vadim99808.constants.ObjectiveType;
import org.bukkit.Material;

public class ObjectiveBlockFertilize extends Objective{
    private ObjectiveType objectiveType = ObjectiveType.BLOCK_FERTILIZE;
    private Material material;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
}
