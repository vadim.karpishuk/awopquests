package awopquests.vadim99808.entity;

import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public class ItemMap {
    private ItemStack itemStack;
    private Optional<Integer> maxAmount;
    private Optional<Integer> minAmount;
    private Optional<Integer> chance;
    private String name;

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public Optional<Integer> getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Optional<Integer> maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Optional<Integer> getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Optional<Integer> minAmount) {
        this.minAmount = minAmount;
    }

    public Optional<Integer> getChance() {
        return chance;
    }

    public void setChance(Optional<Integer> chance) {
        this.chance = chance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
