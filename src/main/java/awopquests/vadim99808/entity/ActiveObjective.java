package awopquests.vadim99808.entity;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.entity.objective.Objective;
import awopquests.vadim99808.events.ObjectiveDoneEvent;
import awopquests.vadim99808.utils.EventCaller;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class ActiveObjective extends Thread {
    private Objective objective;
    private int remainingTime;
    private UUID uuid;
    private Optional<Integer> amount;
    private Map<Player, Integer> activePlayerAmountMap;
    private Map<Player, Integer> passedPlayerAmountMap;
    private boolean finished;
    private boolean started;

    public ActiveObjective() {
        activePlayerAmountMap = new HashMap<>();
        passedPlayerAmountMap = new HashMap<>();
        finished = false;
        started = false;
        uuid = UUID.randomUUID();
    }

    public void run() {
        while (remainingTime > 0 && !finished) {
            try {
                Thread.sleep(1000);
                remainingTime = remainingTime - 1;
            } catch (InterruptedException e) {
                return;
            }
        }
        ObjectiveDoneEvent objectiveDoneEvent = new ObjectiveDoneEvent(this);
        EventCaller eventCaller = new EventCaller(objectiveDoneEvent);
        eventCaller.runTask(AWOPQuests.getInstance());
        finished = true;
    }

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }

    public int getRemainingTime(){
        return remainingTime;
    }

    public void setRemainingTime(int remainingTime){
        this.remainingTime = remainingTime;
    }

    public UUID getUuid() {
        return uuid;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Map<Player, Integer> getActivePlayerAmountMap() {
        return activePlayerAmountMap;
    }

    public Map<Player, Integer> getPassedPlayerAmountMap() {
        return passedPlayerAmountMap;
    }

    public Optional<Integer> getAmount() {
        return amount;
    }

    public void setAmount(Optional<Integer> amount) {
        this.amount = amount;
    }
}
