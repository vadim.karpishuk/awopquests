package awopquests.vadim99808.entity;

import java.util.UUID;

public class ActiveQuest {

    private Quest quest;
    private ActiveObjective activeObjective;
    private UUID uuid;
    private boolean started;
    private boolean finished;

    public ActiveQuest(){
        uuid = UUID.randomUUID();
        started = false;
        finished = false;
    }

    public Quest getQuest() {
        return quest;
    }

    public void setQuest(Quest quest) {
        this.quest = quest;
    }

    public UUID getUuid() {
        return uuid;
    }

    public ActiveObjective getActiveObjective() {
        return activeObjective;
    }

    public void setActiveObjective(ActiveObjective activeObjective) {
        this.activeObjective = activeObjective;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
