package awopquests.vadim99808.entity;


import awopquests.vadim99808.entity.objective.Objective;

import java.util.List;
import java.util.Optional;

public class Quest extends EntityAbstract {

    private Objective objective;
    private String appearMessage;
    private String doneMessage;
    private String disappearMessage;
    private Optional<String> awardsMessage;
    private Optional<String> permission;
    private Optional<Integer> chance;
    private Optional<List<Award>> awardList;

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }

    public String getAppearMessage() {
        return appearMessage;
    }

    public void setAppearMessage(String appearMessage) {
        this.appearMessage = appearMessage;
    }

    public String getDoneMessage() {
        return doneMessage;
    }

    public void setDoneMessage(String doneMessage) {
        this.doneMessage = doneMessage;
    }

    public String getDisappearMessage() {
        return disappearMessage;
    }

    public void setDisappearMessage(String disappearMessage) {
        this.disappearMessage = disappearMessage;
    }

    public Optional<String> getPermission() {
        return permission;
    }

    public void setPermission(Optional<String> permission) {
        this.permission = permission;
    }

    public Optional<Integer> getChance() {
        return chance;
    }

    public void setChance(Optional<Integer> chance) {
        this.chance = chance;
    }

    public Optional<List<Award>> getAwardList() {
        return awardList;
    }

    public void setAwardList(Optional<List<Award>> awardList) {
        this.awardList = awardList;
    }

    public Optional<String> getAwardsMessage() {
        return awardsMessage;
    }

    public void setAwardsMessage(Optional<String> awardsMessage) {
        this.awardsMessage = awardsMessage;
    }
}
