package awopquests.vadim99808.events;

import awopquests.vadim99808.entity.ActiveObjective;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class ObjectiveDoneEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private ActiveObjective activeObjective;

    public ObjectiveDoneEvent(ActiveObjective activeObjective){
        this.activeObjective = activeObjective;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public ActiveObjective getActiveObjective(){
        return activeObjective;
    }

}
