package awopquests.vadim99808.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import awopquests.vadim99808.entity.ActiveQuest;

public class QuestAppearEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private ActiveQuest activeQuest;

    public QuestAppearEvent(ActiveQuest activeQuest){
        this.activeQuest = activeQuest;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public ActiveQuest getActiveQuest() {
        return activeQuest;
    }
}
