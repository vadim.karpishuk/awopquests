package awopquests.vadim99808.configurationLoaders;

import awopquests.vadim99808.constants.LogLevel;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.logger.Logger;
import awopquests.vadim99808.storages.MainConfigurationStorage;

import java.io.File;
import java.io.IOException;

public class MainConfigurationLoader {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private MainConfigurationStorage mainConfigurationStorage = MainConfigurationStorage.getInstance();

    private String spawnSection = "Spawn";
    private String delay = "Delay";
    private String chance = "Chance";
    private String loglevel = "LogLevel";

    public void loadAll(){
        File file = new File(plugin.getMainDirectory(), "config.yml");
        if(!file.exists()){
            if(!createConfiguration()){
                Logger.getInstance().logErrorWithLoadingConfig(file.getName());
                return;
            }
        }else{
            if(!validateConfiguration()){
                Logger.getInstance().logErrorWithLoadingConfig(file.getName());
                return;
            }
        }

        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        mainConfigurationStorage.setSpawnDelay(fileConfiguration.getInt(spawnSection + "." + delay));
        mainConfigurationStorage.setSpawnChance(fileConfiguration.getInt(spawnSection + "." + chance));
        mainConfigurationStorage.setLogLevel(LogLevel.valueOf(fileConfiguration.getString(loglevel)));

        saveFile(fileConfiguration, file);
    }

    private boolean createConfiguration(){
        File file = new File(plugin.getMainDirectory(), "config.yml");
        try {
            file.createNewFile();
        } catch (IOException e) {
            Logger.getInstance().logErrorWithCreatingConfig(file.getName());
            return false;
        }
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        ConfigurationSection spawnConfigurationSection = fileConfiguration.createSection(spawnSection);
        spawnConfigurationSection.set(delay, mainConfigurationStorage.getSpawnDelay());
        spawnConfigurationSection.set(chance, mainConfigurationStorage.getSpawnChance());
        fileConfiguration.set(loglevel, mainConfigurationStorage.getLogLevel().toString());

        return saveFile(fileConfiguration, file);
    }

    private boolean validateConfiguration(){
        File file = new File(plugin.getMainDirectory(), "config.yml");
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        if(!fileConfiguration.contains(spawnSection + "." + delay)){
            fileConfiguration.set(spawnSection + "." + delay, mainConfigurationStorage.getSpawnDelay());
        }
        if(!fileConfiguration.contains(spawnSection + "." + chance)){
            fileConfiguration.set(spawnSection + "." + chance, mainConfigurationStorage.getSpawnChance());
        }
        if(!fileConfiguration.contains(loglevel)){
            fileConfiguration.set(loglevel, mainConfigurationStorage.getLogLevel().toString());
        }

        return saveFile(fileConfiguration, file);
    }

    private boolean saveFile(FileConfiguration fileConfiguration, File file){
        try {
            fileConfiguration.save(file);
        } catch (IOException e) {
            Logger.getInstance().logErrorWithSavingConfig(file.getName());
            return false;
        }
        return true;
    }

}
