package awopquests.vadim99808.configurationLoaders;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.entity.Award;
import awopquests.vadim99808.entity.objective.Objective;
import awopquests.vadim99808.entity.Quest;
import awopquests.vadim99808.logger.Logger;
import awopquests.vadim99808.storages.AwardStorage;
import awopquests.vadim99808.storages.ObjectiveStorage;
import awopquests.vadim99808.storages.QuestStorage;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class QuestConfigurationLoader implements ConfigurationLoader<Quest> {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    public void loadAll(){
        File questsDirectory = plugin.getQuestsDirectory();
        File[] files = questsDirectory.listFiles();
        int count = 0;
        if(files == null){
            plugin.getLogger().info("There are no quests to load!");
            return;
        }
        for(File file: files){
            Quest quest = loadFromFile(file);
            if(quest != null){
                QuestStorage.getInstance().getQuestMap().put(quest.getName(), quest);
                count++;
            }
        }
        plugin.getLogger().info("Loaded " + count + " quests!");
    }

    public Quest loadFromFile(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        String name;
        String objectiveString;
        Objective objective;
        String appearMessage;
        String disappearMessage;
        String doneMessage;
        Optional<String> awardsMessage;
        Optional<String> permission;
        Optional<Integer> chance;
        Optional<List<Award>> optionalAwardList;
        List<Award> awardList;
        List<String> awardNames;
        if(fileConfiguration.contains("Name")){
            name = fileConfiguration.getString("Name");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Name", file);
            return null;
        }
        if(fileConfiguration.contains("Objective")){
            objectiveString = fileConfiguration.getString("Objective");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Objective", file);
            return null;
        }
        if(fileConfiguration.contains("AppearMessage")){
            appearMessage = fileConfiguration.getString("AppearMessage");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("AppearMessage", file);
            return null;
        }
        if(fileConfiguration.contains("DisappearMessage")){
            disappearMessage = fileConfiguration.getString("DisappearMessage");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("DisappearMessage", file);
            return null;
        }
        if(fileConfiguration.contains("DoneMessage")){
            doneMessage = fileConfiguration.getString("DoneMessage");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("DoneMessage", file);
            return null;
        }
        if(fileConfiguration.contains("AwardsMessage")){
            awardsMessage = Optional.of(fileConfiguration.getString("AwardsMessage").replaceAll("&([0-9a-f])", "§$1"));
        }else{
            awardsMessage = Optional.empty();
        }
        if(fileConfiguration.contains("Permission")) {
            permission = Optional.of(fileConfiguration.getString("Permission"));
        }else{
            permission = Optional.empty();
        }
        if(fileConfiguration.contains("Chance")){
           chance = Optional.of(fileConfiguration.getInt("Chance"));
        }else{
            chance = Optional.empty();
        }
        if(fileConfiguration.contains("Awards")){
            awardNames = fileConfiguration.getStringList("Awards");
        }else{
            awardNames = new ArrayList<>();
        }
        awardList = findAwardsByNames(awardNames);
        if(awardList.size() != 0){
            optionalAwardList = Optional.of(awardList);
        }else{
            optionalAwardList = Optional.empty();
        }
        objective = findObjectiveByName(objectiveString);
        if(objective == null){
            Logger.getInstance().logNotFoundObject("Objective", objectiveString, file);
            return null;
        }
        Quest quest = new Quest();
        quest.setAppearMessage(appearMessage.replaceAll("&([0-9a-f])", "§$1"));
        quest.setDisappearMessage(disappearMessage.replaceAll("&([0-9a-f])", "§$1"));
        quest.setDoneMessage(doneMessage.replaceAll("&([0-9a-f])", "§$1"));
        quest.setName(name);
        quest.setChance(chance);
        quest.setPermission(permission);
        quest.setObjective(objective);
        quest.setUuid(UUID.randomUUID());
        quest.setAwardList(optionalAwardList);
        quest.setAwardsMessage(awardsMessage);
        return quest;
    }

    private Objective findObjectiveByName(String name){
        Objective objective;
        try{
            objective = ObjectiveStorage.getInstance().getObjectiveMap().get(name);
        }catch (NullPointerException exception){
            objective = null;
        }
        return objective;
    }

    private List<Award> findAwardsByNames(List<String> awardName){
        List<Award> awardList = new ArrayList<>();
        for(String name: awardName){
            if(AwardStorage.getInstance().getAwardMap().containsKey(name)){
                awardList.add(AwardStorage.getInstance().getAwardMap().get(name));
            }
        }
        return awardList;
    }

}
