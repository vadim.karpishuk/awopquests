package awopquests.vadim99808.configurationLoaders;

import awopquests.vadim99808.entity.EntityAbstract;

import java.io.File;

public interface ConfigurationLoader<T extends EntityAbstract>{

    void loadAll();

    T loadFromFile(File file);

}
