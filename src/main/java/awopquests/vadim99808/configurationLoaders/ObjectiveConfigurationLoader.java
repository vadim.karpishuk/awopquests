package awopquests.vadim99808.configurationLoaders;

import awopquests.vadim99808.constants.LevelChangeType;
import awopquests.vadim99808.constants.ObjectiveType;
import awopquests.vadim99808.constants.ObjectiveWinType;
import awopquests.vadim99808.constants.RegionRule;
import awopquests.vadim99808.entity.objective.*;
import awopquests.vadim99808.logger.SysLog;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.logger.Logger;
import awopquests.vadim99808.storages.ObjectiveStorage;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.*;

public class ObjectiveConfigurationLoader implements ConfigurationLoader<Objective>{

    private AWOPQuests plugin = AWOPQuests.getInstance();

    public void loadAll(){
        File objectiveDirectory = plugin.getObjectiveDirectory();
        File[] files = objectiveDirectory.listFiles();
        int count = 0;
        if(files == null){
            plugin.getLogger().info("There are no objectives to load!");
            return;
        }
        for(File file: files){
            Objective objective = loadFromFile(file);
            if(objective != null) {
                ObjectiveStorage.getInstance().getObjectiveMap().put(objective.getName(), objective);
                count++;
            }
        }
        plugin.getLogger().info("Loaded " + count + " objectives!");
    }

    public Objective loadFromFile(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        ObjectiveType objectiveType;
        String name;
        Optional<Integer> maxAmount = Optional.empty();
        Optional<Integer> minAmount = Optional.empty();
        Optional<Integer> seconds = Optional.empty();
        Optional<Integer> possiblePassedPlayerAmount = Optional.empty();
        Objective objective = null;
        List<World> worldList;
        List<String> worldListStrings;
        ObjectiveWinType objectiveWinType;
        RegionRule regionRule;
        Optional<List<String>> regionNames;
        if(fileConfiguration.contains("Name")){
            name = fileConfiguration.getString("Name");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Name", file);
            return null;
        }
        if(fileConfiguration.contains("ObjectiveType")){
            objectiveType = getObjectiveTypeFromString(fileConfiguration.getString("ObjectiveType"));
            if(objectiveType == null){
                plugin.getLogger().warning("Unknown ObjectiveType in " + file.getName() + " file! Loading of it aborted!");
                return null;
            }
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("ObjectiveType", file);
            return null;
        }
        if(fileConfiguration.contains("WinType")){
            objectiveWinType = getObjectiveWinTypeFromString(fileConfiguration.getString("WinType"));
            if(objectiveWinType == null){
                plugin.getLogger().warning("Unknown WinType in " + file.getName() + " file! Loading of it aborted");
                return null;
            }
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("WinType", file);
            return null;
        }
        if(fileConfiguration.contains("RegionRule")){
            regionRule = getRegionRuleFromString(fileConfiguration.getString("RegionRule"));
            if(regionRule == null){
                plugin.getLogger().warning("Unknown RegionRule in " + file.getName() + " file! Loading of it aborted");
                return null;
            }
        }else{
            regionRule = RegionRule.INCLUDE_ALL;
        }
        if(fileConfiguration.contains("RegionNames")){
            regionNames = Optional.of(fileConfiguration.getStringList("RegionNames"));
        }else{
            regionNames = Optional.empty();
        }
        if(regionRule.equals(RegionRule.STRICT) && !regionNames.isPresent()){
            plugin.getLogger().warning("If RegionRule set to STRICT, you should specify regions! Loading of objective in file " + file.getName() + " aborted!");
            return null;
        }
        if(objectiveWinType.equals(ObjectiveWinType.BY_MAX_AMOUNT_IN_TIME)){
            if(!loadTime(fileConfiguration, file).isPresent()){
                return null;
            }else{
                seconds = loadTime(fileConfiguration, file);
            }
        }else if(objectiveWinType.equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
            if(!loadMaxAmount(fileConfiguration, file).isPresent()){
                return null;
            }else{
                maxAmount = loadMaxAmount(fileConfiguration, file);
            }
            if(!loadMinAmount(fileConfiguration, file).isPresent()){
                return null;
            }else{
                minAmount = loadMinAmount(fileConfiguration, file);
            }
            if(!isAmountsValid(maxAmount.get(), minAmount.get())){
                plugin.getLogger().warning("Max and min amount specified wrongly in " + file.getName() + "! Loading of file aborted!");
                return null;
            }
            if(!loadTime(fileConfiguration, file).isPresent()){
                return null;
            }else{
                seconds = loadTime(fileConfiguration, file);
            }
            if(!loadPossiblePassedPlayerAmount(fileConfiguration, file).isPresent()){
                return null;
            }else{
                possiblePassedPlayerAmount = loadPossiblePassedPlayerAmount(fileConfiguration, file);
            }
        }
        if(fileConfiguration.contains("Worlds")){
            worldListStrings = fileConfiguration.getStringList("Worlds");
            worldList = loadWorldsFromStrings(worldListStrings);
            if(worldList.size() == 0){
                Logger.getInstance().logAbsentParamsWithAbort("Worlds", file);
                return null;
            }
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Worlds", file);
            return null;
        }
        if(objectiveType.equals(ObjectiveType.KILL)){
            objective = loadObjectiveKill(file);
        }
        if(objectiveType.equals(ObjectiveType.BREAK)){
            objective = loadObjectiveBreak(file);
        }
        if(objectiveType.equals(ObjectiveType.BREAK_ITEM)){
            objective = loadObjectiveBreakItem(file);
        }
        if(objectiveType.equals(ObjectiveType.FISH)){
            objective = loadObjectiveFish(file);
        }
        if(objectiveType.equals(ObjectiveType.CONSUME)){
            objective = loadObjectiveConsume(file);
        }
        if(objectiveType.equals(ObjectiveType.EGG_THROW)){
            objective = loadObjectiveEggThrow(file);
        }
        if(objectiveType.equals(ObjectiveType.SHEAR)){
            objective = loadObjectiveShear(file);
        }
        if(objectiveType.equals(ObjectiveType.LEVEL_CHANGE)){
            objective = loadObjectiveLevelChange(file);
        }
        if(objectiveType.equals(ObjectiveType.BLOCK_FERTILIZE)){
            objective = loadObjectiveBlockFertilize(file);
        }
        if(objectiveType.equals(ObjectiveType.ENCHANT_ITEM)){
            objective = loadObjectiveEnchantItem(file);
        }
        
        if(objective == null){
            return null;
        }
        objective.setMaxAmount(maxAmount);
        objective.setMinAmount(minAmount);
        objective.setSeconds(seconds.get());
        objective.setWorldList(worldList);
        objective.setName(name);
        objective.setUuid(UUID.randomUUID());
        objective.setAmountOfPossiblePassedPlayers(possiblePassedPlayerAmount);
        objective.setObjectiveWinType(objectiveWinType);
        objective.setRegionRule(regionRule);
        objective.setRegionNames(regionNames);
        return objective;
    }

    private ObjectiveType getObjectiveTypeFromString(String string){
        try {
            return ObjectiveType.valueOf(string.toUpperCase());
        }catch (IllegalArgumentException exception){
            return null;
        }
    }

    private ObjectiveWinType getObjectiveWinTypeFromString(String string){
        try {
            return ObjectiveWinType.valueOf(string.toUpperCase());
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    private RegionRule getRegionRuleFromString(String string){
        try {
            return RegionRule.valueOf(string.toUpperCase());
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    private ObjectiveKill loadObjectiveKill(File file){
        EntityType entityType = loadEntityType(file);
        if(entityType == null){
            return null;
        }
        ObjectiveKill objectiveKill = new ObjectiveKill();
        objectiveKill.setEntityType(entityType);
        objectiveKill.setItemHeld(loadItemHeld(file));
        objectiveKill.setObjectiveType(ObjectiveType.KILL);
        return objectiveKill;
    }

    private ObjectiveBreak loadObjectiveBreak(File file){
        Material material = loadMaterial(file);
        if(material == null){
            return null;
        }
        ObjectiveBreak objectiveBreak = new ObjectiveBreak();
        objectiveBreak.setMaterial(material);
        objectiveBreak.setItemHeld(loadItemHeld(file));
        objectiveBreak.setObjectiveType(ObjectiveType.BREAK);
        return objectiveBreak;
    }

    private ObjectiveEnchantItem loadObjectiveEnchantItem(File file){
        Material material = loadMaterial(file);
        if(material == null){
            return null;
        }
        ObjectiveEnchantItem objectiveEnchantItem = new ObjectiveEnchantItem();
        objectiveEnchantItem.setMaterial(material);
        objectiveEnchantItem.setEnchantsMap(loadEnchantments(file));
        objectiveEnchantItem.setObjectiveType(ObjectiveType.ENCHANT_ITEM);
        return objectiveEnchantItem;
    }

    private ObjectiveFish loadObjectiveFish(File file){
        Material material = loadMaterial(file);
        if(material == null){
            return null;
        }
        ObjectiveFish objectiveFish = new ObjectiveFish();
        objectiveFish.setMaterial(material);
        objectiveFish.setItemHeld(loadItemHeld(file));
        objectiveFish.setObjectiveType(ObjectiveType.FISH);
        return objectiveFish;
    }

    private ObjectiveBreakItem loadObjectiveBreakItem(File file){
        Material material = loadMaterial(file);
        if(material == null){
            return null;
        }
        ObjectiveBreakItem objectiveBreakItem = new ObjectiveBreakItem();
        objectiveBreakItem.setMaterial(material);
        objectiveBreakItem.setItemHeld(loadItemHeld(file));
        objectiveBreakItem.setObjectiveType(ObjectiveType.BREAK_ITEM);
        return objectiveBreakItem;
    }

    private ObjectiveBlockFertilize loadObjectiveBlockFertilize(File file){
        Material material = loadMaterial(file);
        if(material == null){
            return null;
        }
        ObjectiveBlockFertilize objectiveBlockFertilize = new ObjectiveBlockFertilize();
        objectiveBlockFertilize.setMaterial(material);
        objectiveBlockFertilize.setObjectiveType(ObjectiveType.BLOCK_FERTILIZE);
        return objectiveBlockFertilize;
    }

    private ObjectiveConsume loadObjectiveConsume(File file){
        Material material = loadMaterial(file);
        if(material == null){
            return null;
        }
        ObjectiveConsume objectiveConsume = new ObjectiveConsume();
        objectiveConsume.setMaterial(material);
        objectiveConsume.setItemHeld(loadItemHeld(file));
        objectiveConsume.setObjectiveType(ObjectiveType.CONSUME);
        return objectiveConsume;
    }

    private ObjectiveEggThrow loadObjectiveEggThrow(File file){
        ObjectiveEggThrow objectiveEggThrow = new ObjectiveEggThrow();
        objectiveEggThrow.setObjectiveType(ObjectiveType.EGG_THROW);
        return objectiveEggThrow;
    }

    private ObjectiveShear loadObjectiveShear(File file){
        EntityType entityType = loadEntityType(file);
        if(entityType == null){
            return null;
        }
        ObjectiveShear objectiveShear = new ObjectiveShear();
        objectiveShear.setEntityType(entityType);
        objectiveShear.setItemHeld(loadItemHeld(file));
        objectiveShear.setDyeColor(loadColor(file));
        objectiveShear.setObjectiveType(ObjectiveType.SHEAR);
        return objectiveShear;
    }

    private ObjectiveLevelChange loadObjectiveLevelChange(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        LevelChangeType levelChangeType;
        if(fileConfiguration.contains("LevelChangeType")){
            try {
                levelChangeType = LevelChangeType.valueOf(fileConfiguration.getString("LevelChangeType"));
            }catch (IllegalArgumentException e){
                plugin.getLogger().warning("Unknown LevelChangeType in file " + file.getName() + ". Loading of it aborted!");
                return null;
            }
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("LevelChangeType", file);
            return null;
        }
        ObjectiveLevelChange objectiveLevelChange = new ObjectiveLevelChange();
        objectiveLevelChange.setLevelChangeType(levelChangeType);
        objectiveLevelChange.setObjectiveType(ObjectiveType.LEVEL_CHANGE);
        return objectiveLevelChange;
    }

    private Material loadMaterial(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        Material material;
        if(fileConfiguration.contains("Material")){
            material = Material.valueOf(fileConfiguration.getString("Material").toUpperCase());
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Material", file);
            return null;
        }
        return material;
    }

    private Optional<ItemStack> loadItemHeld(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        ItemStack itemStack;
        Material material;
        String displayName = null;
        int amount;
        Set<String> enchantments = new HashSet<>();
        ConfigurationSection configurationSection;
        if(fileConfiguration.contains("ItemHeld")){
            configurationSection = fileConfiguration.getConfigurationSection("ItemHeld");
        }else{
            return Optional.empty();
        }
        if(configurationSection.contains("Material")){
            material = Material.valueOf(configurationSection.getString("Material"));
        }else{
            plugin.getLogger().warning("Material in ItemHeld not specified in file: " + file.getName());
            return Optional.empty();
        }
        if(configurationSection.contains("DisplayName")){
            displayName = configurationSection.getString("DisplayName");
        }
        if(configurationSection.contains("Enchantments")){
            enchantments = configurationSection.getConfigurationSection("Enchantments").getKeys(false);
        }
        itemStack = new ItemStack(material);
        ItemMeta itemMeta = itemStack.getItemMeta();
        if(displayName != null) {
            itemMeta.setDisplayName(displayName);
        }
        for(String enchantment: enchantments){
            if(Enchantment.getByName(enchantment) != null){
                SysLog.debug("Enchantment loaded: " + Enchantment.getByName(enchantment) + ", level: " + configurationSection.getInt("Enchantments." + enchantment));
                itemMeta.addEnchant(Enchantment.getByName(enchantment), configurationSection.getInt("Enchantments." + enchantment), true);
            }
        }
        itemStack.setItemMeta(itemMeta);
        return Optional.of(itemStack);
    }

    private Optional<Map<Enchantment, Integer>> loadEnchantments(File file){
        Optional<Map<Enchantment, Integer>> optionalEnchantmentIntegerMap = Optional.empty();
        Map<Enchantment, Integer> enchantmentIntegerMap = new HashMap<>();
        Set<String> enchantments;
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        if(fileConfiguration.contains("Enchantments")){
            enchantments = fileConfiguration.getConfigurationSection("Enchantments").getKeys(false);
        }else{
            return optionalEnchantmentIntegerMap;
        }
        for(String enchantment: enchantments){
            if(Enchantment.getByName(enchantment) != null){
                SysLog.debug("Enchantment loaded: " + Enchantment.getByName(enchantment) + ", level: " + fileConfiguration.getInt("Enchantments." + enchantment));
                enchantmentIntegerMap.put(Enchantment.getByName(enchantment), fileConfiguration.getInt("Enchantments" + enchantment));
            }
        }
        optionalEnchantmentIntegerMap = Optional.of(enchantmentIntegerMap);
        return optionalEnchantmentIntegerMap;
    }

    private Optional<DyeColor> loadColor(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        Optional<DyeColor> dyeColor = Optional.empty();
        if(fileConfiguration.contains("Color")){
            try {
                dyeColor = Optional.of(DyeColor.valueOf(fileConfiguration.getString("Color").toUpperCase()));
            }catch (IllegalArgumentException e){
                plugin.getLogger().warning("Unknown Color in file " + file);
            }
        }
        return dyeColor;
    }

    private EntityType loadEntityType(File file){
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        EntityType entityType;
        if(fileConfiguration.contains("EntityType")){
            entityType = EntityType.valueOf(fileConfiguration.getString("EntityType").toUpperCase());
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("EntityType", file);
            return null;
        }
        return entityType;
    }

    private List<World> loadWorldsFromStrings(List<String> worldListStrings){
        List<World> worldList = new ArrayList<>();
        for(String string: worldListStrings){
            if(plugin.getServer().getWorld(string) == null){
                Logger.getInstance().logUnknownWorld(string);
                continue;
            }
            worldList.add(plugin.getServer().getWorld(string));
        }
        return worldList;
    }

    private Optional<Integer> loadMaxAmount(FileConfiguration fileConfiguration, File file){
        if(fileConfiguration.contains("MaxAmount")){
            return Optional.of(fileConfiguration.getInt("MaxAmount"));
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("MaxAmount", file);
            return Optional.empty();
        }
    }

    private Optional<Integer> loadMinAmount(FileConfiguration fileConfiguration, File file){
        if(fileConfiguration.contains("MinAmount")){
            return Optional.of(fileConfiguration.getInt("MinAmount"));
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("MinAmount", file);
            return Optional.empty();
        }
    }


    private Optional<Integer> loadTime(FileConfiguration fileConfiguration, File file){
        if(fileConfiguration.contains("Time")){
            return Optional.of(fileConfiguration.getInt("Time"));
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Time", file);
            return Optional.empty();
        }
    }

    private Optional<Integer> loadPossiblePassedPlayerAmount(FileConfiguration fileConfiguration, File file){
        if(fileConfiguration.contains("PossiblePassedPlayerAmount")){
            return Optional.of(fileConfiguration.getInt("PossiblePassedPlayerAmount"));
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("PossiblePassedPlayerAmount", file);
            return Optional.empty();
        }
    }

    private boolean isAmountsValid(int maxAmount, int minAmount){
        return maxAmount >= minAmount;
    }

}
