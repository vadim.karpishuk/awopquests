package awopquests.vadim99808.configurationLoaders;

import awopquests.vadim99808.logger.SysLog;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitAdapter;
import io.lumine.xikage.mythicmobs.items.MythicItem;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.entity.Award;
import awopquests.vadim99808.entity.ItemMap;
import awopquests.vadim99808.logger.Logger;
import awopquests.vadim99808.storages.AwardStorage;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.*;

public class AwardConfigurationLoader implements ConfigurationLoader<Award> {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    @Override
    public void loadAll() {
        File awardDirectory = plugin.getAwardDirectory();
        File[] files = awardDirectory.listFiles();
        if(files == null){
            plugin.getLogger().info("There are no awards to load!");
            return;
        }
        for(File file: files){
            Award award = loadFromFile(file);
            if(award != null){
                AwardStorage.getInstance().getAwardMap().put(award.getName(), award);
            }
        }
        plugin.getLogger().info("Loaded " + AwardStorage.getInstance().getAwardMap().size() + " awards!");
    }

    @Override
    public Award loadFromFile(File file) {
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        String name;
        Optional<Integer> maxMoney;
        Optional<Integer> minMoney;
        Optional<Integer> moneyChance;
        Optional<List<ItemMap>> optionalItemMapList;
        Optional<Integer> maxXpLevel;
        Optional<Integer> minXpLevel;
        Optional<Integer> xpChance;
        if(fileConfiguration.contains("Name")){
            name = fileConfiguration.getString("Name");
        }else{
            Logger.getInstance().logAbsentParamsWithAbort("Name", file);
            return null;
        }
        if(fileConfiguration.contains("MaxMoney")){
            maxMoney = Optional.of(fileConfiguration.getInt("MaxMoney"));
        }else{
            maxMoney = Optional.empty();
        }
        if(fileConfiguration.contains("MinMoney")){
            minMoney = Optional.of(fileConfiguration.getInt("MinMoney"));
        }else{
            minMoney = Optional.empty();
        }
        if(fileConfiguration.contains("MoneyChance")){
            moneyChance = Optional.of(fileConfiguration.getInt("MoneyChance"));
        }else{
            moneyChance  = Optional.empty();
        }
        if(fileConfiguration.contains("MaxXpLevel")){
            maxXpLevel = Optional.of(fileConfiguration.getInt("MaxXpLevel"));
        }else{
            maxXpLevel = Optional.empty();
        }
        if(fileConfiguration.contains("MinXpLevel")){
            minXpLevel = Optional.of(fileConfiguration.getInt("MinXpLevel"));
        }else{
            minXpLevel = Optional.empty();
        }
        if(fileConfiguration.contains("XpChance")){
            xpChance  = Optional.of(fileConfiguration.getInt("XpChance"));
        }else{
            xpChance = Optional.empty();
        }
        if(fileConfiguration.contains("Items")){
            ConfigurationSection configurationSection = fileConfiguration.getConfigurationSection("Items");
            List<ItemMap> itemMapList = parseItems(configurationSection);
            if(itemMapList.isEmpty()){
                optionalItemMapList = Optional.empty();
            }else{
                optionalItemMapList = Optional.of(itemMapList);
            }
        }else{
            optionalItemMapList = Optional.empty();
        }
        Award award = new Award();
        award.setItemMapList(optionalItemMapList);
        award.setMaxMoney(maxMoney);
        award.setMinMoney(minMoney);
        award.setMoneyChance(moneyChance);
        award.setMaxXpLevel(maxXpLevel);
        award.setMinXpLevel(minXpLevel);
        award.setXpChance(xpChance);
        award.setName(name);
        award.setUuid(UUID.randomUUID());
        if(isAwardValid(award)){
            return award;
        }else{
            return null;
        }
    }

    private List<ItemMap> parseItems(ConfigurationSection configurationSection){
        Set<String> itemsStringSet = configurationSection.getKeys(false);
        List<ItemMap> itemMapList = new ArrayList<>();
        for(String string: itemsStringSet){
            ConfigurationSection itemConfigurationSection = configurationSection.getConfigurationSection(string);
            ItemMap itemMap = parseItem(itemConfigurationSection);
            if(itemMap != null){
                itemMapList.add(itemMap);
            }
        }
        return itemMapList;
    }

    private ItemMap parseItem(ConfigurationSection configurationSection){
        Optional<Integer> maxAmount;
        Optional<Integer> minAmount;
        Optional<Integer> chance;
        ItemStack itemStack;
        String stringMaterialType;
        String name = configurationSection.getName();
        if(configurationSection.contains("MaxAmount")){
            maxAmount = Optional.of(configurationSection.getInt("MaxAmount"));
        }else{
            maxAmount = Optional.empty();
        }
        if(configurationSection.contains("MinAmount")){
            minAmount = Optional.of(configurationSection.getInt("MinAmount"));
        }else{
            minAmount = Optional.empty();
        }
        if(configurationSection.contains("Chance")){
            chance = Optional.of(configurationSection.getInt("Chance"));
        }else{
            chance = Optional.empty();
        }
        if(configurationSection.contains("Type")){
            stringMaterialType = configurationSection.getString("Type");
        }else{
            Logger.getInstance().logAbsentParameterInAwardsWithAbort("Type");
            return null;
        }
        itemStack = parseItemStackFromTypeString(stringMaterialType);
        if(itemStack == null){
            return null;
        }
        ItemMap itemMap = new ItemMap();
        itemMap.setChance(chance);
        itemMap.setItemStack(itemStack);
        itemMap.setMaxAmount(maxAmount);
        itemMap.setMinAmount(minAmount);
        itemMap.setName(name);
        if(isItemMapValid(itemMap)){
            return itemMap;
        }else{
            return null;
        }
    }

    private ItemStack parseItemStackFromTypeString(String typeString){
        ItemStack itemStack;
        SysLog.debug("Trying to parse item with type: " + typeString);
        if(Material.getMaterial(typeString.toUpperCase()) != null){
            itemStack = new ItemStack(Material.getMaterial(typeString.toUpperCase()));
            SysLog.debug("From type " + typeString + " got ItemStack with type " + itemStack.getType());
            return itemStack;
        }else{
            if(plugin.getMythicMobs() != null){
                Optional<MythicItem> optionalMythicItem = MythicMobs.inst().getItemManager().getItem(typeString);
                if(optionalMythicItem.isPresent()){
                    SysLog.debug("From type " + typeString + " got MythicItem with type " + BukkitAdapter.adapt(optionalMythicItem.get().generateItemStack(1)).getType());
                    return (BukkitAdapter.adapt(optionalMythicItem.get().generateItemStack(1)));
                }else{
                    Logger.getInstance().logUnknownMythicItem(typeString);
                    return null;
                }
            }else{
                Logger.getInstance().logCannotLoadMythicItemDueToOffline(typeString);
                return null;
            }
        }
    }

    private boolean isAwardValid(Award award){
        if(award.getMinMoney().isPresent() && !award.getMaxMoney().isPresent()){
            plugin.getLogger().warning("MaxMoney should be specified with MinMoney! Loading of " + award.getName() + " award aborted!");
            return false;
        }
        if(award.getMinXpLevel().isPresent() && !award.getMaxXpLevel().isPresent()){
            plugin.getLogger().warning("MaxXpLevel should be specified with MinXpLevel! Loading of " + award.getName() + " award aborted!");
            return false;
        }
        if(award.getMinMoney().isPresent() && award.getMaxMoney().isPresent()){
            if(award.getMinMoney().get() > award.getMaxMoney().get()){
                plugin.getLogger().warning("MinMoney should be lower than MaxMoney! Loading of " + award.getName() + " award aborted!");
                return false;
            }
        }
        if(award.getMinXpLevel().isPresent() && award.getMaxXpLevel().isPresent()){
            if(award.getMinXpLevel().get() > award.getMaxXpLevel().get()){
                plugin.getLogger().warning("MinXpLevel should be lower than MaxXpLevel! Loading of " + award.getName() + " award aborted!");
                return false;
            }
        }
        if(award.getMoneyChance().isPresent() && (award.getMoneyChance().get() < 0 || award.getMoneyChance().get() > 100)){
            plugin.getLogger().warning("MoneyChance should be between 0 and 100! Loading of " + award.getName() + " award aborted!");
            return false;
        }
        if(award.getXpChance().isPresent() && (award.getXpChance().get() < 0 || award.getXpChance().get() > 100)){
            plugin.getLogger().warning("MoneyChance should be between 0 and 100! Loading of " + award.getName() + " award aborted!");
            return false;
        }
        return true;
    }

    private boolean isItemMapValid(ItemMap itemMap){
        if(itemMap.getMinAmount().isPresent() && !itemMap.getMaxAmount().isPresent()){
            plugin.getLogger().warning("MaxAmount should be specified with MinAmount! Loading of award with " + itemMap.getName() + " ItemMap aborted!");
            return false;
        }
        if(itemMap.getMinAmount().isPresent() && itemMap.getMaxAmount().isPresent()){
            if(itemMap.getMaxAmount().get() < itemMap.getMinAmount().get()){
                plugin.getLogger().warning("MaxAmount should be greater than MinAmount. Loading of item " + itemMap.getName() + " aborted!");
                return false;
            }
        }
        if(itemMap.getChance().isPresent() && (itemMap.getChance().get() < 0 || itemMap.getChance().get() > 100)){
            plugin.getLogger().warning("Chance of ItemMap with name " + itemMap.getName() + " should be between 0 and 100! Loading of award with it aborted!");
            return false;
        }
        return true;
    }

}
