package awopquests.vadim99808.commandExecutors;

import awopquests.vadim99808.entity.Quest;
import awopquests.vadim99808.storages.QuestStorage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.service.QuestService;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class QuestStartCommandExecutor implements CommandExecutor, TabCompleter {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private QuestService questService = plugin.getQuestService();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!commandSender.hasPermission("awopquests.queststart")){
            commandSender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
            return false;
        }
        String name = null;
        if(strings.length != 0){
            name = strings[0];
        }
        if(name != null){
            if(questService.startQuestByName(name)){
                return true;
            }else{
                commandSender.sendMessage(ChatColor.YELLOW + "Cannot spawn quest with name " + name + ". Look into log.");
                return false;
            }
        }else{
            if(questService.startRandomQuest()){
                return true;
            }else{
                commandSender.sendMessage(ChatColor.YELLOW + "Some problems with spawning random quest!");
                return false;
            }
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        Set<Map.Entry<String, Quest>> questSet = QuestStorage.getInstance().getQuestMap().entrySet();
        List<String> questNames = questSet.stream().map(Map.Entry::getKey).collect(Collectors.toList());
        List<String> offeredNames;
        if(strings.length == 1){
            offeredNames = questNames.stream().filter(string -> string.toLowerCase().startsWith(strings[0].toLowerCase())).collect(Collectors.toList());
            return offeredNames;
        }
        return null;
    }
}
