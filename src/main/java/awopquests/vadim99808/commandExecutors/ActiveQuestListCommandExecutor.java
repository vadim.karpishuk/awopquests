package awopquests.vadim99808.commandExecutors;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.service.BroadcastService;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import awopquests.vadim99808.entity.ActiveQuest;
import awopquests.vadim99808.storages.QuestStorage;

import java.util.List;

public class ActiveQuestListCommandExecutor implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender.hasPermission("awopquest.mod.activequestlist")){
            List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
            commandSender.sendMessage(ChatColor.LIGHT_PURPLE + "[" + ChatColor.DARK_AQUA + "List of active quests" + ChatColor.DARK_PURPLE + "]");
            int i = 1;
            for(ActiveQuest activeQuest : activeQuestList){
                commandSender.sendMessage(i + ". Name: " + ChatColor.GREEN + activeQuest.getQuest().getName() + ChatColor.WHITE + ", remaining time: " + ChatColor.RED + activeQuest.getActiveObjective().getRemainingTime());
                i++;
            }
            return true;
        }
        if(commandSender.hasPermission("awopquest.player.activequestlist")){
            List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
            commandSender.sendMessage(ChatColor.LIGHT_PURPLE + "[" + ChatColor.DARK_AQUA + "List of active quests" + ChatColor.DARK_PURPLE + "]");
            int i = 1;
            for(ActiveQuest activeQuest: activeQuestList){
                commandSender.sendMessage(i + ". " + AWOPQuests.getInstance().getBroadcastService().formatAppearingMessage(activeQuest) + " (remaining: " + activeQuest.getActiveObjective().getRemainingTime() + ")");
                i++;
            }
            return true;
        }
        commandSender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
        return false;
    }
}
