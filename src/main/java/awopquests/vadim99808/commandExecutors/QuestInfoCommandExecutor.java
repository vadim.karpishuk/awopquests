package awopquests.vadim99808.commandExecutors;

import awopquests.vadim99808.entity.ActiveQuest;
import awopquests.vadim99808.constants.ObjectiveWinType;
import awopquests.vadim99808.storages.QuestStorage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class QuestInfoCommandExecutor implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!commandSender.hasPermission("awopquest.activequestlist")){
            commandSender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
            return false;
        }
        if(strings.length == 0){
            commandSender.sendMessage(ChatColor.RED + "Please, specify name of active quest!");
            return false;
        }
        String name = strings[0];
        ActiveQuest activeQuestFinded = null;
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        for(ActiveQuest activeQuest: activeQuestList){
            if(activeQuest.getQuest().getName().equals(name)){
                activeQuestFinded = activeQuest;
                break;
            }
        }
        if(activeQuestFinded == null){
            commandSender.sendMessage(ChatColor.RED + "There is no active quest with name " + name + ".");
            return false;
        }
        commandSender.sendMessage("Quest name: " + ChatColor.GREEN + activeQuestFinded.getQuest().getName());
        commandSender.sendMessage("Objective: " + ChatColor.DARK_GREEN + activeQuestFinded.getQuest().getObjective().getName());
        commandSender.sendMessage("Objective type: " + ChatColor.DARK_GREEN + activeQuestFinded.getQuest().getObjective().getObjectiveType());
        commandSender.sendMessage("Objective win type: " + ChatColor.YELLOW + activeQuestFinded.getQuest().getObjective().getObjectiveWinType());
        if(activeQuestFinded.getQuest().getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
            commandSender.sendMessage("Amount to win: " + ChatColor.YELLOW + activeQuestFinded.getActiveObjective().getAmount().get());
        }
        commandSender.sendMessage("Remaining time: " + ChatColor.RED + activeQuestFinded.getActiveObjective().getRemainingTime());
        String activePlayerList = "Active players: ";
        Set<Map.Entry<Player, Integer>> activePlayerSetEntry = activeQuestFinded.getActiveObjective().getActivePlayerAmountMap().entrySet();
        for(Map.Entry<Player, Integer> playerIntegerEntry: activePlayerSetEntry){
            activePlayerList = activePlayerList.concat(playerIntegerEntry.getKey().getName() + " " + playerIntegerEntry.getValue() + "/" + activeQuestFinded.getActiveObjective().getAmount() + ", ");
        }
        commandSender.sendMessage(activePlayerList);
        String passedPlayerList = "Passed players: ";
        Set<Map.Entry<Player, Integer>> passedPlayerSet = activeQuestFinded.getActiveObjective().getPassedPlayerAmountMap().entrySet();
        for(Map.Entry<Player, Integer> playerIntegerEntry: passedPlayerSet){
            passedPlayerList = passedPlayerList.concat(playerIntegerEntry.getKey().getName() + ", ");
        }
        commandSender.sendMessage(ChatColor.GREEN + passedPlayerList);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        List<String> nameList = activeQuestList.stream().map(activeQuest -> activeQuest.getQuest().getName()).collect(Collectors.toList());
        List<String> offeredNames;
        if(strings.length == 1){
            offeredNames = nameList.stream().filter(name -> name.toLowerCase().startsWith(strings[0].toLowerCase())).collect(Collectors.toList());
            return offeredNames;
        }
        return null;
    }
}
