package awopquests.vadim99808.commandExecutors;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.entity.ActiveQuest;
import awopquests.vadim99808.service.QuestService;
import awopquests.vadim99808.storages.QuestStorage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.List;
import java.util.stream.Collectors;

public class QuestStopCommandExecutor implements CommandExecutor, TabCompleter {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private QuestService questService = plugin.getQuestService();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!commandSender.hasPermission("awopquests.queststop")){
            commandSender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
            return false;
        }
        if(strings.length == 0){
            commandSender.sendMessage(ChatColor.RED + "Please, specify quest to stop it!");
            return false;
        }
        String questName = strings[0];
        ActiveQuest activeQuest = questService.findActiveQuestByName(questName);
        if(activeQuest == null){
            commandSender.sendMessage(ChatColor.RED + "There is no active quest with name " + questName + "!");
            return false;
        }
        questService.stopActiveQuest(activeQuest);
        commandSender.sendMessage(ChatColor.DARK_GREEN + "Active quest with name " + activeQuest.getQuest().getName() + " stopped!");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        List<String> nameList = activeQuestList.stream().map(activeQuest -> activeQuest.getQuest().getName()).collect(Collectors.toList());
        List<String> offeredNames;
        if(strings.length == 1){
            offeredNames = nameList.stream().filter(name -> name.toLowerCase().startsWith(strings[0].toLowerCase())).collect(Collectors.toList());
            return offeredNames;
        }
        return null;
    }
}
