package awopquests.vadim99808.commandExecutors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import awopquests.vadim99808.AWOPQuests;

public class QuestReloadCommandExecutor implements CommandExecutor {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!commandSender.hasPermission("awopquests.reload")){
            commandSender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
            return false;
        }
        commandSender.sendMessage(ChatColor.GREEN + "Reloading...");
        plugin.reloadConfigurations();
        commandSender.sendMessage(ChatColor.GREEN + "Reloading complete.");
        return true;
    }
}
