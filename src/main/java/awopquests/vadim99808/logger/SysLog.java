package awopquests.vadim99808.logger;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.constants.LogLevel;
import awopquests.vadim99808.storages.MainConfigurationStorage;

public class SysLog {

    public static void info(String message){
        AWOPQuests.getInstance().getLogger().info(message);
    }

    public static void debug(String message){
        if(MainConfigurationStorage.getInstance().getLogLevel().equals(LogLevel.DEBUG)){
            AWOPQuests.getInstance().getLogger().info("[DEBUG]: " + message);
        }
    }

}
