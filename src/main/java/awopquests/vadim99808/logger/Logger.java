package awopquests.vadim99808.logger;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.entity.Quest;

import java.io.File;

public class Logger {

    private static Logger instance;
    private AWOPQuests plugin;

    private Logger(){
        plugin = AWOPQuests.getInstance();
    }

    public static Logger getInstance(){
        if(instance == null){
            instance = new Logger();
        }
        return instance;
    }

    public void logAbsentParamsWithAbort(String name, File file){
        plugin.getLogger().warning(name + " param is absent in " + file.getName() + " file! Loading of it aborted!");
    }

    public void logNotFoundObject(String nameOfParam, String nameOfNotFoundObject, File file){
        plugin.getLogger().warning(nameOfParam + " param not found by value " + nameOfNotFoundObject + " in " + file.getName() + " file! Loading of it aborted!");
    }

    public void logErrorWithCreatingConfig(String name){
        plugin.getLogger().warning("Error with creating " + name + " file!");
    }

    public void logErrorWithSavingConfig(String name){
        plugin.getLogger().warning("Error with saving " + name + " file!");
    }

    public void logErrorWithLoadingConfig(String name){
        plugin.getLogger().warning("Error with loading " + name + " file!");
    }

    public void logQuestNotSpawnedDueToSameActiveQuest(String name){
        plugin.getLogger().info("Quest " + name + " cannot appear because same quest is active.");
    }

    public void logToManyTriesToSpawnQuest(){
        plugin.getLogger().info("There are too many tries to spawn quest. Spawn of any aborted.");
    }

    public void logCannotSpawnDueToChanceAlgorithm(){
        plugin.getLogger().warning("Cannot choose quest to spawn due to little chances for each!");
    }

    public void logUnknownWorld(String name){
        plugin.getLogger().warning("Unknown world with name: " + name + ".");
    }

    public void logCannotLoadMythicItemDueToOffline(String name){
        plugin.getLogger().warning("Cannot load mythic item with name " + name + " because MythicMobs plugin is not hooked.");
    }

    public void logUnknownMythicItem(String name){
        plugin.getLogger().warning("Unknown MythicItem with name " + name);
    }

    public void logAbsentParameterInAwardsWithAbort(String name){
        plugin.getLogger().warning(name + " parameter is absent in award configuration! Loading of award aborted!");
    }

    public void logCannotFindAwardInQuest(String questName, String awardName){
        plugin.getLogger().info("Cannot find " + awardName + " award in " + questName + " quest!");
    }

    public void logSameQuestAlreadyStarted(String questName){
        plugin.getLogger().warning("Cannot start quest " + questName + " cause same quest already started!");
    }

    public void logNoQuestWithName(String name){
        plugin.getLogger().warning("There is no quest with name " + name + ".");
    }

}
