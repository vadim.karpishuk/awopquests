package awopquests.vadim99808.service;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.constants.LevelChangeType;
import awopquests.vadim99808.constants.ObjectiveType;
import awopquests.vadim99808.constants.ObjectiveWinType;
import awopquests.vadim99808.constants.RegionRule;
import awopquests.vadim99808.entity.*;
import awopquests.vadim99808.entity.objective.*;
import awopquests.vadim99808.logger.SysLog;
import awopquests.vadim99808.storages.QuestStorage;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFertilizeEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class ObjectiveService {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    protected boolean startActiveObjective(ActiveObjective activeObjective){
        activeObjective.setStarted(true);
        activeObjective.start();
        return true;
    }

    public void aggregateKillObjective(EntityDeathEvent entityDeathEvent){
        LivingEntity victim = entityDeathEvent.getEntity();
        Player player = entityDeathEvent.getEntity().getKiller();
        if(player == null){
            return;
        }
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.KILL, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveKill objectiveKill = (ObjectiveKill) activeObjective.getObjective();
            if(objectiveKill.getEntityType().equals(victim.getType())){
                if(objectiveKill.getItemHeld().isPresent() && !isEqualsItemStacks(player.getInventory().getItemInMainHand(), objectiveKill.getItemHeld().get())) {
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateBreakObjective(BlockBreakEvent blockBreakEvent){
        Block block = blockBreakEvent.getBlock();
        Player player = blockBreakEvent.getPlayer();
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.BREAK, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveBreak objectiveBreak = (ObjectiveBreak) activeObjective.getObjective();
            if(objectiveBreak.getMaterial().equals(block.getType())){
                if(objectiveBreak.getItemHeld().isPresent() && !isEqualsItemStacks(player.getInventory().getItemInMainHand(), objectiveBreak.getItemHeld().get())) {
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateFishObjective(PlayerFishEvent playerFishEvent){
        Entity entity = playerFishEvent.getCaught();
        Player player = playerFishEvent.getPlayer();
        if(!playerFishEvent.getState().equals(PlayerFishEvent.State.CAUGHT_FISH)){
            return;
        }
        Item item = (Item) entity;
        SysLog.debug("Aggregating fish event. Player: " + player.getName() + ", type: " + item.getItemStack().getType());
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.FISH, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveFish objectiveFish = (ObjectiveFish) activeObjective.getObjective();
            if(objectiveFish.getMaterial().equals(item.getItemStack().getType())){
                if(objectiveFish.getItemHeld().isPresent() && !isEqualsItemStacks(player.getInventory().getItemInMainHand(), objectiveFish.getItemHeld().get())) {
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateBreakItemObjective(PlayerItemBreakEvent playerItemBreakEvent){
        Player player = playerItemBreakEvent.getPlayer();
        ItemStack itemStack = playerItemBreakEvent.getBrokenItem();
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.BREAK_ITEM, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveBreakItem objectiveBreakItem = (ObjectiveBreakItem) activeObjective.getObjective();
            if(objectiveBreakItem.getMaterial().equals(itemStack.getType())){
                if(objectiveBreakItem.getItemHeld().isPresent() && !isEqualsItemStacks(player.getInventory().getItemInMainHand(), objectiveBreakItem.getItemHeld().get())) {
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateConsumeObjective(PlayerItemConsumeEvent playerItemConsumeEvent){
        Player player = playerItemConsumeEvent.getPlayer();
        ItemStack itemStack = playerItemConsumeEvent.getItem();
        SysLog.debug("Aggregating consume event, player: " + player.getName() + ", item: " + itemStack.getType());
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.CONSUME, player);
        SysLog.debug("Size of list of active objectives with type: " + ObjectiveType.CONSUME + ", size: " + activeObjectiveList.size());
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveConsume objectiveConsume = (ObjectiveConsume) activeObjective.getObjective();
            SysLog.debug("Item type in active objective: " + objectiveConsume.getMaterial());
            if(objectiveConsume.getMaterial().equals(itemStack.getType())){
                SysLog.debug("Item type equals to item type in active objective");
                if(objectiveConsume.getItemHeld().isPresent() && !isEqualsItemStacks(player.getInventory().getItemInMainHand(), objectiveConsume.getItemHeld().get())) {
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateEggThrowObjective(PlayerEggThrowEvent playerEggThrowEvent){
        Player player = playerEggThrowEvent.getPlayer();
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.EGG_THROW, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveEggThrow objectiveEggThrow = (ObjectiveEggThrow) activeObjective.getObjective();
            aggregatePlayerScoreInActiveObjective(player, activeObjective);
            if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                checkForPassedPlayersByAmount(activeObjective);
                checkActiveObjectiveForComplete(activeObjective);
            }
        }
    }

    public void aggregateShearObjective(PlayerShearEntityEvent playerShearEntityEvent){
        Player player = playerShearEntityEvent.getPlayer();
        Entity entity = playerShearEntityEvent.getEntity();
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.SHEAR, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveShear objectiveShear = (ObjectiveShear) activeObjective.getObjective();
            if(objectiveShear.getEntityType().equals(entity.getType())){
                if(objectiveShear.getItemHeld().isPresent() && !isEqualsItemStacks(player.getInventory().getItemInMainHand(), objectiveShear.getItemHeld().get())) {
                    return;
                }
                if(entity instanceof Sheep && objectiveShear.getDyeColor().isPresent() && !((Sheep)entity).getColor().equals(objectiveShear.getDyeColor().get())){
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateLevelChangeObjective(PlayerLevelChangeEvent playerLevelChangeEvent){
        Player player = playerLevelChangeEvent.getPlayer();
        int newLevel = playerLevelChangeEvent.getNewLevel();
        int oldLevel = playerLevelChangeEvent.getOldLevel();
        int difference = newLevel - oldLevel;
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.LEVEL_CHANGE, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveLevelChange objectiveLevelChange = (ObjectiveLevelChange) activeObjective.getObjective();
            if((difference < 0 && objectiveLevelChange.getLevelChangeType().equals(LevelChangeType.DECREASE)) ||
                    (difference > 0 && objectiveLevelChange.getLevelChangeType().equals(LevelChangeType.INCREASE))){
                aggregatePlayerScoreInActiveObjective(player, activeObjective, Math.abs(difference));
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateBlockFertilizeObjective(BlockFertilizeEvent blockFertilizeEvent){
        Player player = blockFertilizeEvent.getPlayer();
        Block block = blockFertilizeEvent.getBlock();
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.BLOCK_FERTILIZE, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveBlockFertilize objectiveBlockFertilize = (ObjectiveBlockFertilize) activeObjective.getObjective();
            if(objectiveBlockFertilize.getMaterial().equals(block.getType())){
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    public void aggregateEnchantItemObjective(EnchantItemEvent enchantItemEvent){
        Player player = enchantItemEvent.getEnchanter();
        ItemStack itemStack = enchantItemEvent.getItem();
        List<ActiveObjective> activeObjectiveList = getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType.BLOCK_FERTILIZE, player);
        for(ActiveObjective activeObjective: activeObjectiveList){
            ObjectiveEnchantItem objectiveEnchantItem = (ObjectiveEnchantItem) activeObjective.getObjective();
            if(itemStack.getType().equals(objectiveEnchantItem.getMaterial())){
                if(objectiveEnchantItem.getEnchantsMap().isPresent() &&
                        !enchantItemEvent.getEnchantsToAdd().entrySet().containsAll(objectiveEnchantItem.getEnchantsMap().get().entrySet())){
                    return;
                }
                aggregatePlayerScoreInActiveObjective(player, activeObjective);
                if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
                    checkForPassedPlayersByAmount(activeObjective);
                    checkActiveObjectiveForComplete(activeObjective);
                }
            }
        }
    }

    private List<ActiveObjective> getActiveObjectiveListByObjectiveTypeAndPlayer(ObjectiveType objectiveType, Player player){
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        List<ActiveObjective> activeObjectiveList = new ArrayList<>();
        for(ActiveQuest activeQuest: activeQuestList){
            if(activeQuest.getActiveObjective().isStarted()
                    && !activeQuest.getActiveObjective().isFinished()
                    && activeQuest.getActiveObjective().getObjective().getObjectiveType().equals(objectiveType)
                    && (!activeQuest.getQuest().getPermission().isPresent() || player.hasPermission(activeQuest.getQuest().getPermission().get()))
                    && activeQuest.getActiveObjective().getObjective().getWorldList().contains(player.getWorld())){
                activeObjectiveList.add(activeQuest.getActiveObjective());
            }
        }
        return filterByRegionRule(activeObjectiveList, player);
    }

    private List<ActiveObjective> filterByRegionRule(List<ActiveObjective> activeObjectives, Player player){
        List<ActiveObjective> filteredActiveObjectives = new ArrayList<>();
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionManager regionManager = container.get(BukkitAdapter.adapt(player.getWorld()));
        ApplicableRegionSet regionSet = regionManager.getApplicableRegions(BlockVector3.at(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()));
        for(ActiveObjective activeObjective: activeObjectives){
            if(activeObjective.getObjective().getRegionRule().equals(RegionRule.STRICT)){
                for(ProtectedRegion protectedRegion: regionSet.getRegions()){
                    if(activeObjective.getObjective().getRegionNames().get().contains(protectedRegion.getId())){
                        filteredActiveObjectives.add(activeObjective);
                    }
                }
            }
            if(activeObjective.getObjective().getRegionRule().equals(RegionRule.INCLUDE)){
                if(activeObjective.getObjective().getRegionNames().isPresent()){
                    for(ProtectedRegion protectedRegion: regionSet.getRegions()){
                        if(activeObjective.getObjective().getRegionNames().get().contains(protectedRegion.getId())){
                            filteredActiveObjectives.add(activeObjective);
                        }
                    }
                }else{
                    filteredActiveObjectives.add(activeObjective);
                }
            }
            if(activeObjective.getObjective().getRegionRule().equals(RegionRule.INCLUDE_ALL)){
                filteredActiveObjectives.add(activeObjective);
            }
            if(activeObjective.getObjective().getRegionRule().equals(RegionRule.EXCLUDE_ALL) && regionSet.getRegions().size() == 0){
                filteredActiveObjectives.add(activeObjective);
            }
            if(activeObjective.getObjective().getRegionRule().equals(RegionRule.EXCLUDE)){
                if(activeObjective.getObjective().getRegionNames().isPresent()){
                    boolean excluded = false;
                    for(ProtectedRegion protectedRegion: regionSet.getRegions()){
                        if(activeObjective.getObjective().getRegionNames().get().contains(protectedRegion.getId())){
                            excluded = true;
                            break;
                        }
                    }
                    if(!excluded){
                        filteredActiveObjectives.add(activeObjective);
                    }
                }else{
                    filteredActiveObjectives.add(activeObjective);
                }
            }
        }
        return filteredActiveObjectives;
    }

    private void aggregatePlayerScoreInActiveObjective(Player player, ActiveObjective activeObjective){
        if(activeObjective.getPassedPlayerAmountMap().containsKey(player)){
            return;
        }
        if(activeObjective.getActivePlayerAmountMap().containsKey(player)){
            activeObjective.getActivePlayerAmountMap().replace(player, activeObjective.getActivePlayerAmountMap().get(player) + 1);
        }else{
            activeObjective.getActivePlayerAmountMap().put(player, 1);
        }
    }

    private void aggregatePlayerScoreInActiveObjective(Player player, ActiveObjective activeObjective, int amount){
        if(activeObjective.getPassedPlayerAmountMap().containsKey(player)){
            return;
        }
        if(activeObjective.getActivePlayerAmountMap().containsKey(player)){
            activeObjective.getActivePlayerAmountMap().replace(player, activeObjective.getActivePlayerAmountMap().get(player) + amount);
        }else{
            activeObjective.getActivePlayerAmountMap().put(player, amount);
        }
    }


    private void checkForPassedPlayersByAmount(ActiveObjective activeObjective){
        Set<Map.Entry<Player, Integer>> entrySet = activeObjective.getActivePlayerAmountMap().entrySet();
        for(Map.Entry<Player, Integer> entry: entrySet){
            if(entry.getValue() >= activeObjective.getAmount().get()){
                activeObjective.getPassedPlayerAmountMap().put(entry.getKey(), entry.getValue());
                activeObjective.getActivePlayerAmountMap().remove(entry.getKey());
            }
        }
    }

    private void checkActiveObjectiveForComplete(ActiveObjective activeObjective){
        if(activeObjective.getPassedPlayerAmountMap().size() == activeObjective.getObjective().getAmountOfPossiblePassedPlayers().get()){
            activeObjective.setFinished(true);
        }
    }

    public void activeObjectiveFinisher(ActiveObjective activeObjective){
        moveWinnerToPassedPlayers(activeObjective);
        plugin.getQuestService().activeQuestFinisher(
                plugin.getQuestService().findActiveQuestByActiveObjective(activeObjective));
    }

    private void moveWinnerToPassedPlayers(ActiveObjective activeObjective){
        if(activeObjective.getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
            return;
        }
        Set<Map.Entry<Player, Integer>> entrySet = activeObjective.getActivePlayerAmountMap().entrySet();
        Map.Entry<Player, Integer> maxEntry = null;
        for(Map.Entry<Player, Integer> playerIntegerEntry: entrySet){
            if(maxEntry == null){
                maxEntry = playerIntegerEntry;
            }
            if(maxEntry.getValue() < playerIntegerEntry.getValue()){
                maxEntry = playerIntegerEntry;
            }
        }
        if(maxEntry != null){
            activeObjective.getActivePlayerAmountMap().remove(maxEntry.getKey());
            activeObjective.getPassedPlayerAmountMap().put(maxEntry.getKey(), maxEntry.getValue());
        }
    }

    private boolean isEqualsItemStacks(ItemStack playerItemStack, ItemStack objectiveItemStack){
        SysLog.debug("Type of item of player: " + playerItemStack.getType().toString());
        if(!playerItemStack.getType().equals(objectiveItemStack.getType())){
            SysLog.debug("Mismatch of item types");
            return false;
        }
        if(!playerItemStack.getItemMeta().getDisplayName().equals(objectiveItemStack.getItemMeta().getDisplayName())){
            SysLog.debug("Mismatch of display names of items");
            return false;
        }
        SysLog.debug("Enchants in player item: " + playerItemStack.getEnchantments().toString());
        SysLog.debug("Enchants in objective item: " + objectiveItemStack.getEnchantments().toString());
        if(!playerItemStack.getItemMeta().getEnchants().entrySet().containsAll(objectiveItemStack.getItemMeta().getEnchants().entrySet())){
            SysLog.debug("Mismatch of enchants of items");
            return false;
        }
        return true;
    }


}
