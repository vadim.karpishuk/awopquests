package awopquests.vadim99808.service;

import awopquests.vadim99808.entity.ActiveQuest;
import awopquests.vadim99808.entity.AggregatedAward;
import awopquests.vadim99808.constants.ObjectiveWinType;
import awopquests.vadim99808.logger.SysLog;
import org.bukkit.entity.Player;
import awopquests.vadim99808.AWOPQuests;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BroadcastService {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    public void broadcastAboutAppearing(ActiveQuest activeQuest){
        broadcastToAllowedPlayers(activeQuest, formatAppearingMessage(activeQuest));
    }

    public String formatAppearingMessage(ActiveQuest activeQuest){
        String message = activeQuest.getQuest().getAppearMessage();
        if(activeQuest.getActiveObjective().getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
            message = message.replace("<amount>", Integer.toString(activeQuest.getActiveObjective().getAmount().get()));
        }
        message = message.replace("<time>", Integer.toString(activeQuest.getActiveObjective().getObjective().getSeconds()));
        return message;
    }

    public void broadcastAboutDisappearing(ActiveQuest activeQuest){
        String message = activeQuest.getQuest().getDisappearMessage();
        if(activeQuest.getActiveObjective().getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
            message = message.replace("<amount>", Integer.toString(activeQuest.getActiveObjective().getAmount().get()));
        }
        message = message.replace("<time>", Integer.toString(activeQuest.getActiveObjective().getObjective().getSeconds()));
        broadcastToAllowedPlayers(activeQuest, message);
    }

    public void broadcastAboutDone(ActiveQuest activeQuest){
        String message = activeQuest.getQuest().getDoneMessage();
        if(activeQuest.getActiveObjective().getObjective().getObjectiveWinType().equals(ObjectiveWinType.BY_AMOUNT_IN_TIME)){
            message = message.replace("<amount>", Integer.toString(activeQuest.getActiveObjective().getAmount().get()));
            message = message.replace("<player_list>", createPlayerWinString(activeQuest));
        }else{
            List<Map.Entry<Player, Integer>> entryList = new ArrayList<>(activeQuest.getActiveObjective().getPassedPlayerAmountMap().entrySet());
            message = message.replace("<amount>", Integer.toString(entryList.get(0).getValue()));
            message = message.replace("<player>", entryList.get(0).getKey().getName());
        }
        message = message.replace("<time>", Integer.toString(activeQuest.getActiveObjective().getObjective().getSeconds()));
        broadcastToAllowedPlayers(activeQuest, message);
    }

    private String createPlayerWinString(ActiveQuest activeQuest){
        List<Map.Entry<Player, Integer>> entryList = new ArrayList<>(activeQuest.getActiveObjective().getPassedPlayerAmountMap().entrySet());
        String playersString = "";
        for(Map.Entry<Player, Integer> entry: entryList){
            playersString = playersString.concat(entry.getKey().getName() + ", ");
        }
        return playersString.substring(0, playersString.length() - 2);
    }

    private void broadcastToAllowedPlayers(ActiveQuest activeQuest, String message){
        List<Player> playerList = new ArrayList<>(plugin.getServer().getOnlinePlayers());
        for(Player player: playerList){
            if(activeQuest.getQuest().getPermission().isPresent()){
                if(player.hasPermission(activeQuest.getQuest().getPermission().get())) {
                    player.sendMessage(message);
                }
            }else{
                player.sendMessage(message);
            }
        }
    }

    public void broadCastAboutAwards(ActiveQuest activeQuest, List<AggregatedAward> aggregatedAwards){
        if(!activeQuest.getQuest().getAwardsMessage().isPresent()){
            return;
        }
        String message = activeQuest.getQuest().getAwardsMessage().get();
        for(AggregatedAward aggregatedAward: aggregatedAwards){
            String awardMessage = message;
            awardMessage = awardMessage.replace("<player>", aggregatedAward.getPlayer().getName());
            awardMessage = awardMessage.replace("<items>", createItemStackString(aggregatedAward));
            awardMessage = awardMessage.replace("<money>", Integer.toString(aggregatedAward.getMoney()));
            awardMessage = awardMessage.replace("<xpLevel>", Integer.toString(aggregatedAward.getXpLevel()));
            broadcastToAllowedPlayers(activeQuest, awardMessage);
        }
    }

    private String createItemStackString(AggregatedAward aggregatedAward){
        List<ItemStack> itemStackList = aggregatedAward.getItemStackList();
        String message = "";
        for(ItemStack itemStack: itemStackList){
            SysLog.debug("ItemStack in award message: ");
            if(itemStack == null){
                SysLog.debug("ItemStack is null");
            }else {
                SysLog.debug("Type: " + itemStack.getType());
                SysLog.debug("Display name: " + itemStack.getItemMeta().getDisplayName());
                SysLog.debug("Amount: " + itemStack.getAmount());
            }
            message = message.concat(itemStack.getType().name().toLowerCase().replace("_", " ") + " x" + itemStack.getAmount() + ", ");
        }
        return message.substring(0, message.length() - 2);
    }

}
