package awopquests.vadim99808.service;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.entity.AggregatedAward;
import awopquests.vadim99808.entity.Award;
import awopquests.vadim99808.entity.ItemMap;
import awopquests.vadim99808.events.QuestDoneEvent;
import awopquests.vadim99808.logger.SysLog;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.stream.Collectors;

public class AwardService {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    public List<AggregatedAward> aggregateAwardsInDoneQuest(QuestDoneEvent questDoneEvent){
        if(!questDoneEvent.getActiveQuest().getQuest().getAwardList().isPresent()){
            return new ArrayList<>();
        }
        List<Award> awardList = questDoneEvent.getActiveQuest().getQuest().getAwardList().get();
        Set<Map.Entry<Player, Integer>> entrySet = questDoneEvent.getActiveQuest().getActiveObjective().getPassedPlayerAmountMap().entrySet();
        Map<Player, AggregatedAward> aggregatedAwardMap = new HashMap<>();
        for(Award award: awardList){
            for(Map.Entry<Player, Integer> entry: entrySet) {
                AggregatedAward aggregatedAward = new AggregatedAward();
                aggregatedAward.setPlayer(entry.getKey());
                aggregatedAward.setMoney(aggregateMoney(award, entry.getKey()));
                aggregatedAward.setXpLevel(aggregateXpLevel(award, entry.getKey()));
                aggregatedAward.setItemStackList(aggregateItems(award, entry.getKey()));
                if(!aggregatedAwardMap.containsKey(entry.getKey())){
                    SysLog.debug("Aggregated award not contains in map");
                    aggregatedAwardMap.put(entry.getKey(), aggregatedAward);
                }else{
                    SysLog.debug("Aggregated award contains in map, will join old and new");
                    AggregatedAward oldAggregatedAward = aggregatedAwardMap.get(entry.getKey());
                    aggregatedAwardMap.replace(entry.getKey(), joinAggregatedAwards(aggregatedAward, oldAggregatedAward));
                }
            }
        }
        Set<Map.Entry<Player, AggregatedAward>> entries = aggregatedAwardMap.entrySet();
        return entries.stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    private int aggregateMoney(Award award, Player player){
        int moneyToAdd = 0;
        Random random = new Random();
        if(award.getMinMoney().isPresent() && award.getMaxMoney().isPresent()){
            int difference = award.getMaxMoney().get() - award.getMinMoney().get();
            int growth = random.nextInt(difference);
            moneyToAdd = award.getMinMoney().get() + growth;
        }
        if(!award.getMinMoney().isPresent() && award.getMaxMoney().isPresent()){
            moneyToAdd = random.nextInt(award.getMaxMoney().get());
        }
        if(award.getMoneyChance().isPresent()){
            if(random.nextInt(101) > award.getMoneyChance().get()){
                moneyToAdd = 0;
            }
        }
        if(moneyToAdd != 0){
            plugin.getEconomy().depositPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()), moneyToAdd);
        }
        return moneyToAdd;
    }

    private int aggregateXpLevel(Award award, Player player){
        int levelToAdd = 0;
        Random random = new Random();
        if(award.getMinXpLevel().isPresent() && award.getMaxXpLevel().isPresent()){
            int difference = award.getMaxXpLevel().get() - award.getMinXpLevel().get();
            int growth = random.nextInt(difference);
            levelToAdd = award.getMinXpLevel().get() + growth;
        }
        if(!award.getMinXpLevel().isPresent() && award.getMaxXpLevel().isPresent()){
            levelToAdd = random.nextInt(award.getMaxXpLevel().get());
        }
        if(award.getXpChance().isPresent()){
            if(random.nextInt(101) > award.getXpChance().get()){
                levelToAdd = 0;
            }
        }
        if(levelToAdd != 0){
            player.giveExpLevels(levelToAdd);
        }
        return levelToAdd;
    }

    private List<ItemStack> aggregateItems(Award award, Player player){
        if(!award.getItemMapList().isPresent()){
            return new ArrayList<>();
        }
        List<ItemMap> itemMapList = award.getItemMapList().get();
        List<ItemStack> itemStackList = new ArrayList<>();
        for(ItemMap itemMap: itemMapList){
            ItemStack itemStack = aggregateItemMap(itemMap, player);
            if(itemStack != null) {
                itemStackList.add(itemStack);
            }
        }
        return itemStackList;
    }

    private ItemStack aggregateItemMap(ItemMap itemMap, Player player){
        ItemStack itemStack = itemMap.getItemStack();
        Random random = new Random();
        int amount = 0;
        if(itemMap.getMinAmount().isPresent() && itemMap.getMaxAmount().isPresent()){
            int difference = itemMap.getMaxAmount().get() - itemMap.getMinAmount().get();
            int growth = random.nextInt(difference + 1);
            amount = itemMap.getMinAmount().get() + growth;
        }
        if(!itemMap.getMinAmount().isPresent() && itemMap.getMaxAmount().isPresent()){
            amount = random.nextInt(itemMap.getMaxAmount().get() + 1);
        }
        if(!itemMap.getMinAmount().isPresent() && !itemMap.getMaxAmount().isPresent()){
            amount = 1;
        }
        if(itemMap.getChance().isPresent()){
            if(random.nextInt(101) > itemMap.getChance().get()){
                amount = 0;
            }
        }
        if(amount != 0){
            itemStack.setAmount(amount);
        }else{
            return null;
        }
        ItemStack itemStackCloneToReturn = itemStack.clone();
        while(itemStack.getAmount() > itemStack.getMaxStackSize()){
            ItemStack itemStackClone = itemStack.clone();
            itemStackClone.setAmount(itemStackClone.getMaxStackSize());
            Optional<Integer> optionalInteger = findClosestEmptySlot(player);
            if(optionalInteger.isPresent()){
                player.getInventory().setItem(optionalInteger.get(), itemStackClone);
            }else{
                player.getWorld().dropItem(player.getLocation(), itemStackClone);
            }
            itemStack.setAmount(itemStack.getAmount() - itemStack.getMaxStackSize());
        }
        Optional<Integer> optionalInteger = findClosestEmptySlot(player);
        if(optionalInteger.isPresent()){
            player.getInventory().setItem(optionalInteger.get(), itemStack);
        }else{
            player.getWorld().dropItem(player.getLocation(), itemStack);
        }
        return itemStackCloneToReturn;
    }

    private Optional<Integer> findClosestEmptySlot(Player player){
        for(int i = 0; i <= 35; i++){
            if(player.getInventory().getItem(i) == null){
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    private AggregatedAward joinAggregatedAwards(AggregatedAward newAggregatedAward, AggregatedAward oldAggregatedAward){
        if(!oldAggregatedAward.getPlayer().equals(newAggregatedAward.getPlayer())){
            return null;
        }
        AggregatedAward aggregatedAward = new AggregatedAward();
        aggregatedAward.setPlayer(newAggregatedAward.getPlayer());
        aggregatedAward.setXpLevel(newAggregatedAward.getXpLevel() + oldAggregatedAward.getXpLevel());
        aggregatedAward.setMoney(newAggregatedAward.getMoney() + oldAggregatedAward.getMoney());
        List<ItemStack> itemStackList = new ArrayList<>();
        itemStackList.addAll(newAggregatedAward.getItemStackList());
        itemStackList.addAll(oldAggregatedAward.getItemStackList());
        aggregatedAward.setItemStackList(itemStackList);
        return aggregatedAward;
    }

}
