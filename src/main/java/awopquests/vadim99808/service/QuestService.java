package awopquests.vadim99808.service;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.constants.ObjectiveType;
import awopquests.vadim99808.constants.ObjectiveWinType;
import awopquests.vadim99808.entity.*;
import awopquests.vadim99808.entity.objective.Objective;
import awopquests.vadim99808.events.QuestAppearEvent;
import awopquests.vadim99808.events.QuestDisappearEvent;
import awopquests.vadim99808.events.QuestDoneEvent;
import awopquests.vadim99808.logger.Logger;
import awopquests.vadim99808.storages.QuestStorage;
import awopquests.vadim99808.utils.EventCaller;
import org.bukkit.Bukkit;

import java.util.*;

public class QuestService {

    private AWOPQuests plugin = AWOPQuests.getInstance();

    private boolean startActiveQuest(ActiveQuest activeQuest){
        QuestAppearEvent questAppearEvent = new QuestAppearEvent(activeQuest);
        EventCaller eventCaller = new EventCaller(questAppearEvent);
        eventCaller.runTask(plugin);
        activeQuest.setStarted(true);
        QuestStorage.getInstance().getActiveQuestList().add(activeQuest);
        plugin.getObjectiveService().startActiveObjective(activeQuest.getActiveObjective());
        return true;
    }

    //TODO: check same active quests and determine tries in config
    public boolean startRandomQuest(){
        Quest quest = null;
        for(int i = 0; i < 10; i++) {
            quest = chooseRandomQuest();
            if(quest != null){
                break;
            }
        }
        if(quest == null){
            Logger.getInstance().logToManyTriesToSpawnQuest();
            return false;
        }
        ActiveQuest activeQuest = new ActiveQuest();
        ActiveObjective activeObjective = new ActiveObjective();
        activeObjective.setObjective(quest.getObjective());
        activeObjective.setRemainingTime(quest.getObjective().getSeconds());
        activeObjective.setAmount(generateRandomAmountForActiveObjective(quest.getObjective()));
        activeQuest.setQuest(quest);
        activeQuest.setActiveObjective(activeObjective);
        return startActiveQuest(activeQuest);
    }

    public boolean startQuestByName(String name){
        if(!QuestStorage.getInstance().getQuestMap().containsKey(name)){
            return false;
        }
        Quest quest = QuestStorage.getInstance().getQuestMap().get(name);
        if(isSameQuestAlreadyActive(quest)){
            Logger.getInstance().logSameQuestAlreadyStarted(quest.getName());
            return false;
        }
        ActiveObjective activeObjective = new ActiveObjective();
        activeObjective.setObjective(quest.getObjective());
        activeObjective.setRemainingTime(quest.getObjective().getSeconds());
        activeObjective.setAmount(generateRandomAmountForActiveObjective(quest.getObjective()));
        ActiveQuest activeQuest = new ActiveQuest();
        activeQuest.setQuest(quest);
        activeQuest.setActiveObjective(activeObjective);
        return startActiveQuest(activeQuest);
    }

    private Quest chooseRandomQuest(){
        Set<Map.Entry<String, Quest>> questEntrySet = QuestStorage.getInstance().getQuestMap().entrySet();
        List<Map.Entry<String, Quest>> entryList = new ArrayList<Map.Entry<String, Quest>>(questEntrySet);
        Random random = new Random();
        Quest quest = null;
        int counter = 0;
        while (counter < 10000){
            quest = entryList.get(random.nextInt(entryList.size())).getValue();
            if(isSameQuestAlreadyActive(quest)){
                continue;
            }
            if(quest.getChance().isPresent() && quest.getChance().get() != 0 && random.nextInt(101) <= quest.getChance().get()){
                break;
            }else{
                if(!quest.getChance().isPresent()){
                    break;
                }
            }
            counter++;
        }
        if(counter >= 10000){
            Logger.getInstance().logCannotSpawnDueToChanceAlgorithm();
            return null;
        }

        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        for(ActiveQuest activeQuest : activeQuestList){
            if(activeQuest.getQuest().equals(quest)){
                return null;
            }
        }
        return quest;
    }

    public List<ActiveQuest> findQuestThreadByObjective(ObjectiveType objectiveType){
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        List<ActiveQuest> filteredActiveQuestList = new ArrayList<ActiveQuest>();
        for(ActiveQuest activeQuest : activeQuestList){
            if(activeQuest.getQuest().getObjective().getObjectiveType().equals(objectiveType)){
                filteredActiveQuestList.add(activeQuest);
            }
        }
        return filteredActiveQuestList;
    }


    private boolean isSameQuestAlreadyActive(Quest quest){
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        for(ActiveQuest activeQuest : activeQuestList){
            if(activeQuest.getQuest().equals(quest)){
                return true;
            }
        }
        return false;
    }

    public void stopAllActiveQuests(){
        List<ActiveQuest> activeQuestList = QuestStorage.getInstance().getActiveQuestList();
        for(ActiveQuest activeQuest : activeQuestList){
            activeQuest.getActiveObjective().interrupt();
        }
    }

    public void stopActiveQuest(ActiveQuest activeQuest){
        activeQuest.getActiveObjective().interrupt();
        activeQuest.setFinished(true);
        QuestStorage.getInstance().getActiveQuestList().remove(activeQuest);
    }

    public void activeQuestFinisher(ActiveQuest activeQuest){
        activeQuest.setFinished(true);
        QuestStorage.getInstance().getActiveQuestList().remove(activeQuest);
        if(activeQuest.getActiveObjective().getPassedPlayerAmountMap().size() == 0){
            QuestDisappearEvent questDisappearEvent = new QuestDisappearEvent(activeQuest);
            EventCaller eventCaller = new EventCaller(questDisappearEvent);
            eventCaller.runTask(plugin);
        }else{
            QuestDoneEvent questDoneEvent = new QuestDoneEvent(activeQuest);
            EventCaller eventCaller = new EventCaller(questDoneEvent);
            eventCaller.runTask(plugin);
        }
    }

    public ActiveQuest findActiveQuestByActiveObjective(ActiveObjective activeObjective){
        return QuestStorage.getInstance().getActiveQuestList().stream().
                filter(activeQuest -> activeQuest.getActiveObjective().
                        equals(activeObjective)).findFirst().orElse(null);
    }

    public ActiveQuest findActiveQuestByName(String activeQuestName){
        return QuestStorage.getInstance().getActiveQuestList().stream().
                filter(activeQuest -> activeQuest.getQuest().getName().
                        equalsIgnoreCase(activeQuestName)).findFirst().orElse(null);
    }

    private Optional<Integer> generateRandomAmountForActiveObjective(Objective objective){
        if(objective.getObjectiveWinType().equals(ObjectiveWinType.BY_MAX_AMOUNT_IN_TIME)){
            return Optional.empty();
        }
        Random random = new Random();
        int diff = objective.getMaxAmount().get() - objective.getMinAmount().get();
        if(diff == 0){
            return objective.getMinAmount();
        }else{
            return Optional.of(random.nextInt(diff) + objective.getMinAmount().get());
        }
    }
}
