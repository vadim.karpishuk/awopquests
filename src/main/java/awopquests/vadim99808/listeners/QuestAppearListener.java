package awopquests.vadim99808.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.events.QuestAppearEvent;
import awopquests.vadim99808.service.BroadcastService;

public class QuestAppearListener implements Listener {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private BroadcastService broadcastService = plugin.getBroadcastService();

    @EventHandler
    public void onQuestAppear(QuestAppearEvent questAppearEvent){
        broadcastService.broadcastAboutAppearing(questAppearEvent.getActiveQuest());
    }
}
