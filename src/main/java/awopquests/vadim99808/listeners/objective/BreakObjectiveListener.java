package awopquests.vadim99808.listeners.objective;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.service.ObjectiveService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BreakObjectiveListener implements Listener {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private ObjectiveService objectiveService = plugin.getObjectiveService();

    @EventHandler
    public void onBreak(BlockBreakEvent blockBreakEvent){
        objectiveService.aggregateBreakObjective(blockBreakEvent);
    }
}
