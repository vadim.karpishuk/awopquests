package awopquests.vadim99808.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.events.QuestDisappearEvent;
import awopquests.vadim99808.service.BroadcastService;

public class QuestDisappearListener implements Listener {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private BroadcastService broadcastService = plugin.getBroadcastService();

    @EventHandler
    public void onQuestDisappear(QuestDisappearEvent questDisappearEvent){
        broadcastService.broadcastAboutDisappearing(questDisappearEvent.getActiveQuest());
    }
}
