package awopquests.vadim99808.listeners;

import awopquests.vadim99808.entity.AggregatedAward;
import awopquests.vadim99808.service.AwardService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.events.QuestDoneEvent;
import awopquests.vadim99808.service.BroadcastService;

import java.util.List;

public class QuestDoneListener implements Listener {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private BroadcastService broadcastService = plugin.getBroadcastService();
    private AwardService awardService = plugin.getAwardService();

    @EventHandler
    public void onQuestDone(QuestDoneEvent questDoneEvent){
        broadcastService.broadcastAboutDone(questDoneEvent.getActiveQuest());
        List<AggregatedAward> aggregatedAwards = awardService.aggregateAwardsInDoneQuest(questDoneEvent);
        broadcastService.broadCastAboutAwards(questDoneEvent.getActiveQuest(), aggregatedAwards);
    }
}
