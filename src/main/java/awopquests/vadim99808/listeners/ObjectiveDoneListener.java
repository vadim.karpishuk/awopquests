package awopquests.vadim99808.listeners;

import awopquests.vadim99808.AWOPQuests;
import awopquests.vadim99808.events.ObjectiveDoneEvent;
import awopquests.vadim99808.service.ObjectiveService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ObjectiveDoneListener implements Listener {

    private AWOPQuests plugin = AWOPQuests.getInstance();
    private ObjectiveService objectiveService = plugin.getObjectiveService();

    @EventHandler
    public void onObjectiveDone(ObjectiveDoneEvent objectiveDoneEvent){
        objectiveService.activeObjectiveFinisher(objectiveDoneEvent.getActiveObjective());
    }

}
