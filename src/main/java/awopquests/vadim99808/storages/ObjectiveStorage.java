package awopquests.vadim99808.storages;

import awopquests.vadim99808.entity.objective.Objective;

import java.util.HashMap;
import java.util.Map;

public class ObjectiveStorage {

    private static ObjectiveStorage objectiveStorage;
    private Map<String, Objective> objectiveMap;

    private ObjectiveStorage(){
        objectiveMap = new HashMap<String, Objective>();
    }

    public static ObjectiveStorage getInstance(){
        if(objectiveStorage == null){
            objectiveStorage = new ObjectiveStorage();
        }
        return objectiveStorage;
    }

    public Map<String, Objective> getObjectiveMap() {
        return objectiveMap;
    }
}
