package awopquests.vadim99808.storages;

import awopquests.vadim99808.entity.Quest;
import awopquests.vadim99808.entity.ActiveQuest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestStorage {
    private static QuestStorage questStorage;
    private Map<String, Quest> questMap;
    private List<ActiveQuest> activeQuestList;

    private QuestStorage(){
        questMap = new HashMap<String, Quest>();
        activeQuestList = new ArrayList<ActiveQuest>();
    }

    public static synchronized QuestStorage getInstance(){
        if(questStorage == null){
            questStorage = new QuestStorage();
        }
        return questStorage;
    }

    public synchronized Map<String, Quest> getQuestMap() {
        return questMap;
    }

    public synchronized List<ActiveQuest> getActiveQuestList() {
        return activeQuestList;
    }
}
