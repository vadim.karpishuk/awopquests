package awopquests.vadim99808.storages;

import awopquests.vadim99808.entity.Award;

import java.util.HashMap;
import java.util.Map;

public class AwardStorage {

    private static AwardStorage awardStorage;
    private Map<String, Award> awardMap;

    private AwardStorage(){
        awardMap = new HashMap<>();
    }

    public static AwardStorage getInstance(){
        if(awardStorage == null){
            awardStorage = new AwardStorage();
        }
        return awardStorage;
    }

    public Map<String, Award> getAwardMap() {
        return awardMap;
    }

    public void setAwardMap(Map<String, Award> awardMap) {
        this.awardMap = awardMap;
    }
}
