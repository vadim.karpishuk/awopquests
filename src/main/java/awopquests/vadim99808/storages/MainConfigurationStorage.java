package awopquests.vadim99808.storages;

import awopquests.vadim99808.constants.LogLevel;

public class MainConfigurationStorage {

    private static MainConfigurationStorage mainConfigurationStorage;
    private int spawnDelay;
    private int spawnChance;
    private LogLevel logLevel;

    private MainConfigurationStorage(){
        spawnDelay = 60;
        spawnChance = 1;
        logLevel = LogLevel.INFO;
    }

    public static MainConfigurationStorage getInstance(){
        if(mainConfigurationStorage == null){
            mainConfigurationStorage = new MainConfigurationStorage();
        }
        return mainConfigurationStorage;
    }

    public int getSpawnDelay() {
        return spawnDelay;
    }

    public void setSpawnDelay(int spawnDelay) {
        this.spawnDelay = spawnDelay;
    }

    public int getSpawnChance() {
        return spawnChance;
    }

    public void setSpawnChance(int spawnChance) {
        this.spawnChance = spawnChance;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }
}
