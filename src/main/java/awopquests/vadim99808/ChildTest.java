package awopquests.vadim99808;

import openyaml.vadim99808.annotations.ConfigField;
import openyaml.vadim99808.annotations.ConfigFile;
import openyaml.vadim99808.annotations.ConfigPath;
import openyaml.vadim99808.annotations.Primary;

import java.io.File;

@ConfigFile
public class ChildTest {

    @ConfigField(name = "Name")
    @Primary
    private String name;

    @ConfigPath
    private File file;

    public ChildTest(){
        this.file = new File(AWOPQuests.getInstance().getMainDirectory(), "testChild");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
